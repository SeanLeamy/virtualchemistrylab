﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using Assets.CustomAssets.Scripts.Manual;

namespace Assets.CustomAssets.Scripts.Parsers
{
    public class ManualParser
    {

        static string getOneDepthElements = "/manual/child::*";
        static string getListElementsAtIndex = "//ul[{0}]/child::*";

        /**
         * @fn  public static List<ManualElement> getManual(string xmlPath)
         *
         * @brief   Gets a manual.
         *
         * @param   xmlPath Full pathname of the XML file.
         *
         * @return  The manual.
         */

        public static List<ManualElement> getManual(string xmlPath)
        {
            List<ManualElement> manualElements = new List<ManualElement>();
            TextAsset textAssest = Resources.Load<TextAsset>(xmlPath);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(textAssest.text);
            XmlNodeList itemNodes = xmlDoc.SelectNodes(getOneDepthElements);
            foreach (XmlNode itemNode in itemNodes)
            {
                string tagName = itemNode.Name;
                ManualElement.Tag tag = ManualElement.identifyTag(tagName);
                // Identify Tag type and create appropriate Element class
                if (tag == ManualElement.Tag.h1 || tag == ManualElement.Tag.p)
                {
                    int attributeCount = itemNode.Attributes.Count;
                    string attributes = "";
                    if (attributeCount > 0)
                        attributes = itemNode.Attributes["style"].Value;
                    TextElement te = new TextElement(tag, itemNode.InnerText, attributes);
                    manualElements.Add(te);
                } else if(tag == ManualElement.Tag.img)
                {
                    int attributeCount = itemNode.Attributes.Count;
                    string height = "";
                    if (attributeCount > 1)
                        height = itemNode.Attributes["height"].Value;
                    ImageElement ie = new ImageElement(tag, itemNode.InnerText, itemNode.Attributes["align"].Value, height);
                    manualElements.Add(ie);
                } else if(tag == ManualElement.Tag.ul)
                {
                    // Get TextElement children of list
                    List<TextElement> innerTextElements = new List<TextElement>();
                    foreach(XmlNode childNode in itemNode.ChildNodes)
                    {
                        string childTagName = childNode.Name;
                        ManualElement.Tag childTag = ManualElement.identifyTag(childTagName);
                        int childAttributeCount = childNode.Attributes.Count;
                        string childAttributes = "";
                        if (childAttributeCount > 0)
                            childAttributes = childNode.Attributes["style"].Value;
                        TextElement te = new TextElement(childTag, childNode.InnerText, childAttributes);
                        innerTextElements.Add(te);
                    }
                    int attributeCount = itemNode.Attributes.Count;
                    string attributes = "";
                    if (attributeCount > 0)
                        attributes = itemNode.Attributes["style"].Value;
                    ListElement le = new ListElement(tag, innerTextElements, attributes);
                    manualElements.Add(le);
                }
            }

            return manualElements;
        }

        /**
         * @fn  public string parameterizedXpath(string xpath, string[] xpathParams)
         *
         * @brief   Parameterized xpath.
         *
         * @param   xpath       The xpath.
         * @param   xpathParams Options for controlling the xpath.
         *
         * @return  A string.
         */

        public string parameterizedXpath(string xpath, string[] xpathParams)
        {
            for(int i=0; i<xpathParams.Length; i++)
            {
                xpath = xpath.Replace("{" + i + "}", xpathParams[i]);
            }
            return xpath;
        }

    }




}
