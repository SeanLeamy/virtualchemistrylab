﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EquipmentMenu : MonoBehaviour {

    float mouseDownTime;
    float reactionTime = 0.2f;
    UIController uiController;
    public GameObject menuGameObject;

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

	void Start () {
        uiController = GameObject.Find("Utils").GetComponent<UIController>();
        menuGameObject = null;
	}

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hitInfo = Physics2D.Raycast(new Vector2(mPos.x, mPos.y), Vector2.zero, Mathf.Infinity);
            bool hitObject = false;
            if (hitInfo)
                if (gameObject.transform == hitInfo.transform)
                    hitObject = true;
          
            if (menuGameObject != null && !hitObject)
            {
                Destroy(menuGameObject);
            }
        }

	}

    /**
     * @fn  void OnMouseDown()
     *
     * @brief   Executes the mouse down action.
     */

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return; // Prevent clicking through UI
        mouseDownTime = Time.time;
    }

    /**
     * @fn  void OnMouseUp()
     *
     * @brief   Executes the mouse up action.
     */

    void OnMouseUp()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return; // Prevent clicking through UI
        float mouseUpTime = Time.time;
        float reaction = mouseUpTime - mouseDownTime;


        if (!isAnyConfigMenuOpen())
        {
            if (reaction <= reactionTime && menuGameObject == null)
            {
                menuGameObject = uiController.createEquipmentMenu(gameObject);
            }
        }
            
            
    }

    /**
     * @fn  bool isAnyConfigMenuOpen()
     *
     * @brief   Queries if any configuration menu is open.
     *
     * @return  True if any configuration menu is open, false if not.
     */

    bool isAnyConfigMenuOpen()
    {
        Equipment[] equipment = Object.FindObjectsOfType<Equipment>();
        foreach(Equipment eqp in equipment)
        {
            if (eqp.getConfigPanel() != null)
                return true;
        }
        return false;
    }

    

}
