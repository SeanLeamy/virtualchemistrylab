﻿using UnityEngine;
using System.Collections;

public class MaterialPanelToggle : MonoBehaviour {

    RectTransform rt;

    /**
     * @fn  void Start()
     *
     * @brief   Use this for initialization.
     */

    void Start()
    {
        rt = gameObject.GetComponent<RectTransform>();
        if (isVisible())
        {
            closeMenu();
        }
    }

    /**
     * @fn  void Update()
     *
     * @brief   Update is called once per frame.
     */

    void Update()
    {

    }

    /**
     * @fn  public bool isVisible()
     *
     * @brief   Query if this object is visible.
     *
     * @return  True if visible, false if not.
     */

    public bool isVisible()
    {
        if (rt.anchoredPosition.x == 0)
            return true;

        return false;
    }

    /**
     * @fn  void closeMenu()
     *
     * @brief   Closes the menu.
     */

    void closeMenu()
    {
        Vector2 aPos = rt.anchoredPosition;
        rt.anchoredPosition = new Vector2(aPos.x - rt.sizeDelta.x, aPos.y);
    }

    /**
     * @fn  void showMenu()
     *
     * @brief   Shows the menu.
     */

    void showMenu()
    {
        Vector2 aPos = rt.anchoredPosition;
        rt.anchoredPosition = new Vector2(aPos.x + rt.sizeDelta.x, aPos.y);
    }

    /**
     * @fn  public void toggleMenu()
     *
     * @brief   Toggle menu.
     */

    public void toggleMenu()
    {
        if (isVisible())
        {
            closeMenu();
        }
        else
        {
            showMenu();
        }
    }

}
