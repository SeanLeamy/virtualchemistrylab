﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonClick : MonoBehaviour, IPointerClickHandler{

    private LabManager labManager;
    private RightClick rightClick;
    private UIController uiController;

    void Start()
    {
        labManager = GameObject.Find("LabManager").GetComponent<LabManager>();
        if (uiController == null)
        {
            uiController = GameObject.Find("Utils").GetComponent<UIController>();
        }
        rightClick = labManager.GetComponent<RightClick>();
    }

    public void OnPointerClick(PointerEventData data)
    {
        Equipment target = rightClick.target;
        if (this.name == "RenameButton")
        {
            Debug.Log("Clicked Rename");
            uiController.createRenameDialogBox();
            rightClick.DestroyExistingMenus();
        }
        if (this.name == "SaveButton")
        {
            Debug.Log("Clicked Save");
            rightClick.DestroyExistingMenus();
        }
        if (this.name == "DuplicateButton")
        {
            Debug.Log("Clicked Duplicate");

            rightClick.DestroyExistingMenus();
        }
        if (this.name == "DeleteButton")
        {
            Debug.Log("Clicked Delete");
            labManager.deleteItem();
            rightClick.DestroyExistingMenus();
        }
        if(this.name == "DeleteAllButton")
        {
            labManager.deleteAllExistingItems();
            rightClick.DestroyExistingMenus();
        }

    }
}