﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class ScrollHandleHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public float hoverAlpha=1;
    public float normalAlpha=0;
    private Color hoverColor;
    private Color originalColor;

    /**
     * @fn  void Start()
     *
     * @brief   Starts this object.
     */

    void Start()
    {
        Image img = gameObject.GetComponent<Image>();
        originalColor = img.color;
        originalColor = new Color(originalColor.r, originalColor.g, originalColor.b, normalAlpha);
        img.color = originalColor;
    }

    /**
     * @fn  public void OnPointerEnter(PointerEventData eventData)
     *
     * @summary <para></para>
     *
     * @param   eventData   Current event data.
     */

    public void OnPointerEnter(PointerEventData eventData)
    {
        Image img = gameObject.GetComponent<Image>();
        originalColor = hoverColor = img.color;
        hoverColor = new Color(hoverColor.r, hoverColor.g, hoverColor.b, hoverAlpha);
        img.color = hoverColor;
    }

    /**
     * @fn  public void OnPointerExit(PointerEventData eventData)
     *
     * @summary <para></para>
     *
     * @param   eventData   Current event data.
     */

    public void OnPointerExit(PointerEventData eventData)
    {
        Image img = gameObject.GetComponent<Image>();
        img.color = originalColor;
    }
}
