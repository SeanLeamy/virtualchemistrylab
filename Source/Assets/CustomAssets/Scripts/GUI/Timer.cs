﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

    GameObject labelTextGO;
    Text labelText;
    GameObject closeButton;
    GameObject closeButtonText;
    GameObject timeTextGO;
    Text timeText;
    GameObject startStopButton;
    Text startStopText;
    GameObject resetButton;

    float time;
    int seconds;
    int minutes;
    int hours;

    string label;

    private int nextUpdate = 1;

    bool timerEnabled;

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

	void Start () {
        labelTextGO = transform.Find("Toolbar/TimerLabel").gameObject;
        labelText = labelTextGO.GetComponent<Text>();
        labelText.text = label;
        closeButton = transform.Find("Toolbar/CloseButton").gameObject;
        closeButtonText = transform.Find("Toolbar/CloseButton/CloseText").gameObject;
        timeTextGO = transform.Find("Time/TimeText").gameObject;
        timeText = timeTextGO.GetComponent<Text>();
        startStopButton = transform.Find("Controls/StartStopButton").gameObject;
        startStopText = startStopButton.GetComponentInChildren<Text>();
        resetButton = transform.Find("Controls/ResetButton").gameObject;
        timerEnabled = false;
        ResetTime();
        UpdateDisplay();
    }

    /**
     * @fn  public void SetLabel(string label)
     *
     * @brief   Sets a label.
     *
     * @param   label   The label.
     */

    public void SetLabel(string label)
    {
        this.label = label;
    }

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

	void Update () {
        //Debug.Log("update");
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            if (timerEnabled)
            {
                //Debug.Log("second update");
                time++;
                UpdateTime();
                UpdateDisplay();
            }
        }
	}

    /**
     * @fn  public void ToggleTimer()
     *
     * @brief   Toggle timer.
     */

    public void ToggleTimer()
    {
        timerEnabled = !timerEnabled;
        UpdateDisplay();
    }

    /**
     * @fn  public void ResetTime()
     *
     * @brief   Resets the time.
     */

    public void ResetTime()
    {
        time = 0;
        timerEnabled = false;
        UpdateTime();
        UpdateDisplay();
    }

    /**
     * @fn  void UpdateTime()
     *
     * @brief   Updates the time.
     */

    void UpdateTime()
    {
        seconds = (int) (time % 60);
        minutes = (int) ((time / 60) % 60);
        hours = (int) ((time / 3600) % 24);
    }

    void UpdateDisplay()
    {
        string timeStr = string.Format("{2}:{1}:{0}", seconds.ToString("D2"), minutes.ToString("D2"), hours.ToString("D2"));
        timeText.text = timeStr;
        if (timerEnabled)
        {
            startStopText.text = "Stop";
        }
        else
        {
            startStopText.text = "Start";
        }
    }

    /**
     * @fn  public void DestroyTimer()
     *
     * @brief   Destroys the timer.
     */

    public void DestroyTimer()
    {
        Destroy(gameObject);
    }

}
