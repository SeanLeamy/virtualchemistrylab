﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;
using Assets.CustomAssets.Scripts.Manual;
using System.Collections.Generic;

public class UIController : MonoBehaviour {


    public Sprite panelBackground;
    public Sprite tooltipBackground;
    public Sprite scrollBarImage;
    public Sprite scrollBarHandleImage;
    public Sprite inputFieldBackground;
    public Sprite buttonImage;
    public Sprite configIcon;
    public Sprite trashIcon;
    public Font textFont;

    private int timerCount = 1;

    private GameObject labManual;
    public GameObject renameDialogBox;
    public GameObject canvas;

    private GameObject labManager;

    private RightClick rightClick;

    private bool canClick;

    // Anchor Points //
    // TopLeft
    public static Vector2 anchorTopLeftMin = new Vector2(0, 1f);
    public static Vector2 anchorTopLeftMax = new Vector2(0, 1f);
    public static Vector2 anchorTopLeftPivot = new Vector2(0, 1f);
    // TopCenter
    public static Vector2 anchorTopCenterMin = new Vector2(0.5f, 1f);
    public static Vector2 anchorTopCenterMax = new Vector2(0.5f, 1f);
    public static Vector2 anchorTopCenterPivot = new Vector2(0.5f, 1f);
    // MiddleCenter
    public static Vector2 anchorMiddleCenterMin = new Vector2(0.5f, 0.5f);
    public static Vector2 anchorMiddleCenterMax = new Vector2(0.5f, 0.5f);
    public static Vector2 anchorMiddleCenterPivot = new Vector2(0.5f, 0.5f);
    // MiddleLeft
    public static Vector2 anchorMiddleLeftMin = new Vector2(0f, 0.5f);
    public static Vector2 anchorMiddleLeftMax = new Vector2(0f, 0.5f);
    public static Vector2 anchorMiddleLeftPivot = new Vector2(0f, 0.5f);
    // MiddleRight
    public static Vector2 anchorMiddleRightMin = new Vector2(1f, 0.5f);
    public static Vector2 anchorMiddleRightMax = new Vector2(1f, 0.5f);
    public static Vector2 anchorMiddleRightPivot = new Vector2(1f, 0.5f);
    // BottomCenter
    public static Vector2 anchorBottomCenterMin = new Vector2(0.5f, 0f);
    public static Vector2 anchorBottomCenterMax = new Vector2(0.5f, 0f);
    public static Vector2 anchorBottomCenterPivot = new Vector2(0.5f, 0f);
    // Stretch all
    public static Vector2 anchorStretchAllMin = new Vector2(0, 0);
    public static Vector2 anchorStretchAllMax = new Vector2(1, 1);
    public static Vector2 anchorStretchAllPivot = new Vector2(0.5f, 0.5f);
    // Stretch Top
    public static Vector2 anchorStretchTopMin = new Vector2(0, 1);
    public static Vector2 anchorStretchTopMax = new Vector2(1, 1);
    public static Vector2 anchorStretchTopPivot = new Vector2(0.5f, 1);
    
    // End Anchor Points


    // Use this for initialization
    void Awake () {
        //textFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void Start()
    {
        labManager = GameObject.Find("LabManager");
        rightClick = labManager.GetComponent<RightClick>();
        
    }

    /**
     * @fn  public GameObject createPanel( string objectName, string windowName, float off_x, float off_y, float w, float h, bool draggable )
     *
     * @brief   Creates a panel.
     *
     * @param   objectName  Name of the object.
     * @param   windowName  Name of the window.
     * @param   off_x       The off x coordinate.
     * @param   off_y       The off y coordinate.
     * @param   w           The width.
     * @param   h           The height.
     * @param   draggable   True if draggable.
     *
     * @return  The new panel.
     */

    public void removeRenameDialogBox()
    {
        renameDialogBox.transform.localPosition = new Vector3(4000 - canvas.transform.position.x, 0 + canvas.transform.position.y, 0);
    }

    public void createRenameDialogBox()
    {
        Text messageText = GameObject.Find("MessageText").GetComponent<Text>();
        InputField renameField = GameObject.Find("RenameField").GetComponent<InputField>();
        messageText.text = string.Format("New name for {0}:", rightClick.target.itemName);
        renameField.text = "";
        // Postion the dialog box to the center of the screen
        RectTransform canvasRT = canvas.transform as RectTransform;
        renameDialogBox.transform.localPosition = new Vector3(canvas.transform.position.x - (canvasRT.rect.width / 2), canvas.transform.position.y - (canvasRT.rect.height / 2), 0);
    }

    public void renameObject()
    {
        Text newNameText = GameObject.Find("RenameText").GetComponent<Text>();
        rightClick.target.itemName = newNameText.text;
        removeRenameDialogBox();
    }

    public GameObject createPanel(
        string objectName, string windowName, 
        float off_x, float off_y, 
        float w, float h,
        bool draggable
        )
    {
        string closeButtonString = "[X]";
        float titleTextX_offset = 0f;
        float titleTextY_offset = -10f;
        float closeButtonY_offset = -6f;
        float closeButtonX_offset = -8f;
        float closeButtonW = 17f;
        float textHeight = 20f;
        float contentAreaStartY_offset = -45f;
        float contentAreaW = w - 20f;
        float contentAreaH = h + (contentAreaStartY_offset - 10f);


        GameObject canvas = GameObject.Find("Canvas");

        if(canvas == null)
        {
            Debug.Log("canvas not found");
        }

        GameObject panel = new GameObject();
        panel.name = objectName;
        panel.transform.SetParent(canvas.transform);
        panel.transform.localPosition = new Vector3(0, 0, 1); // Set to Center of screen
        panel.transform.localPosition = new Vector3(panel.transform.localPosition.x + off_x, panel.transform.localPosition.y + off_y);   
        RectTransform rt = panel.AddComponent<RectTransform>();
        rt.sizeDelta = new Vector2(w, h);
        panel.AddComponent<CanvasRenderer>();
        Image i = panel.AddComponent<Image>();
        i.type = Image.Type.Sliced;
        i.sprite = panelBackground;
        if(draggable)
            panel.AddComponent<DragPanel>();

        GameObject panelTitle = new GameObject();
        panelTitle.name = objectName + "PanelTitle";
        panelTitle.transform.SetParent(panel.transform);
        RectTransform panelTitleRT = panelTitle.AddComponent<RectTransform>();
        // Set to top center anchor
        panelTitleRT.anchorMin = new Vector2(0.5f, 1f);
        panelTitleRT.anchorMax = new Vector2(0.5f, 1f);
        panelTitleRT.pivot = new Vector2(0.5f, 1f);
        // Offset anchored position from top
        panelTitleRT.anchoredPosition = new Vector3(titleTextX_offset, titleTextY_offset, 1);
        // Set Size of title object
        panelTitleRT.sizeDelta = new Vector2(w, textHeight);
        // Add Text to panel
        Text panelTitleText = panelTitle.AddComponent<Text>();
        panelTitleText.text = windowName;
        panelTitleText.alignment = TextAnchor.MiddleCenter;
        panelTitleText.font = textFont;
        panelTitleText.color = Color.black;
        panelTitleText.resizeTextForBestFit = false;
        // Add [x] close button to panel
        GameObject panelCloseButtonGO = new GameObject();
        panelCloseButtonGO.name = objectName + "PanelCloseButton";
        panelCloseButtonGO.transform.SetParent(panel.transform);
        // Add Text GameObject
        GameObject closeButtonTextGO = new GameObject();
        closeButtonTextGO.name = objectName + "CloseButtonText";
        closeButtonTextGO.transform.SetParent(panelCloseButtonGO.transform);
        RectTransform cbtRT = closeButtonTextGO.AddComponent<RectTransform>();
        // Add Content Size Fitter
        ContentSizeFitter cbtCST = closeButtonTextGO.AddComponent<ContentSizeFitter>();
        cbtCST.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        cbtCST.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        // Set to middle center
        cbtRT.anchorMin = new Vector2(0.5f, 0.5f);
        cbtRT.anchorMax = new Vector2(0.5f, 0.5f);
        cbtRT.pivot = new Vector2(0.5f, 0.5f);
        // Offset anchored position from top
        cbtRT.anchoredPosition = new Vector3(0, 0);
        // Add Text component
        Text panelCloseButtonText = closeButtonTextGO.AddComponent<Text>();
        panelCloseButtonText.text = closeButtonString;
        panelCloseButtonText.alignment = TextAnchor.MiddleRight;
        panelCloseButtonText.font = textFont;
        panelCloseButtonText.color = Color.white;
        // Add Button component
        Button panelCloseButton = panelCloseButtonGO.AddComponent<Button>();
        panelCloseButton.interactable = true;
        panelCloseButton.transition = Selectable.Transition.ColorTint;
        ColorBlock cb = panelCloseButton.colors;
        cb.normalColor = Color.black;
        cb.highlightedColor = new Color(0.11f, 0.11f, 0.11f, 1f);
        cb.colorMultiplier = 5f;
        panelCloseButton.colors = cb;
        panelCloseButton.targetGraphic = closeButtonTextGO.GetComponent<Text>();
        // Add RectTransform to panelCloseButton
        RectTransform panelCloseButtonRT = panelCloseButtonGO.AddComponent<RectTransform>();
        // Set to top right anchor
        panelCloseButtonRT.anchorMin = new Vector2(1f, 1f);
        panelCloseButtonRT.anchorMax = new Vector2(1f, 1f);
        panelCloseButtonRT.pivot = new Vector2(1f, 1f);
        // Offset anchored position from top
        panelCloseButtonRT.anchoredPosition = new Vector3(closeButtonX_offset, closeButtonY_offset, 1);
        // Set rectTransform size
        panelCloseButtonRT.sizeDelta = new Vector2(closeButtonW, textHeight);

        // Set Button actions
        panelCloseButton.onClick.AddListener(() => { destoryPanel(panel); });


        // Add Mask and Scroll Object
        GameObject maskAreaGO = new GameObject();
        maskAreaGO.name = objectName + "MaskScrollArea";
        maskAreaGO.transform.SetParent(panel.transform);
        RectTransform mag = maskAreaGO.AddComponent<RectTransform>();
        // Set to top center anchor
        mag.anchorMin = new Vector2(0.5f, 1f);
        mag.anchorMax = new Vector2(0.5f, 1f);
        mag.pivot = new Vector2(0.5f, 1f);
        // Offset anchored position from top
        mag.anchoredPosition = new Vector3(0, contentAreaStartY_offset, 1);
        // Set Size of title object
        mag.sizeDelta = new Vector2(contentAreaW, contentAreaH);
        // Add Image and Mask for overflow
        Image maskImage = maskAreaGO.AddComponent<Image>();
        maskImage.fillCenter = true;
        Mask maskMask = maskAreaGO.AddComponent<Mask>();
        maskMask.showMaskGraphic = false;
        // Add Content Area Object Under Mask and Scroll Object
        GameObject contentAreaGO = new GameObject();
        contentAreaGO.name = objectName + "PanelContentArea";
        contentAreaGO.transform.SetParent(maskAreaGO.transform);
        RectTransform contentAreaRT = contentAreaGO.AddComponent<RectTransform>();
        // Set to top center anchor
        contentAreaRT.anchorMin = new Vector2(0.5f, 1f);
        contentAreaRT.anchorMax = new Vector2(0.5f, 1f);
        contentAreaRT.pivot = new Vector2(0.5f, 1f);
        // Set Size of title object
        contentAreaRT.sizeDelta = new Vector2(contentAreaW, contentAreaH);
        // Set Anchored Position
        contentAreaRT.anchoredPosition = new Vector2(0, 0);
        // Add Content Size Fitter for expandable height
        ContentSizeFitter csf = contentAreaGO.AddComponent<ContentSizeFitter>();
        csf.horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
        csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;


        // Add Scroll Rect to Mask and Scroll
        ScrollRect scrollRect = maskAreaGO.AddComponent<ScrollRect>();
        scrollRect.horizontal = false;
        scrollRect.vertical = true;
        scrollRect.movementType = ScrollRect.MovementType.Clamped;
        scrollRect.scrollSensitivity = 50f;
        scrollRect.content = contentAreaGO.GetComponent<RectTransform>(); // Assign content as Content Area
        scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;
        scrollRect.horizontalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;


        return panel;

    }

    /**
     * @fn  public void addGridLayout( GameObject contentArea, int left, int right, int top, int bottom, float cell_x, float cell_y, float spac_x, float spac_y, GridLayoutGroup.Corner startCorner, GridLayoutGroup.Axis startAxis, TextAnchor childAlignment, GridLayoutGroup.Constraint constraint )
     *
     * @brief   Adds a grid layout.
     *
     * @param   contentArea     The content area.
     * @param   left            The left.
     * @param   right           The right.
     * @param   top             The top.
     * @param   bottom          The bottom.
     * @param   cell_x          The cell x coordinate.
     * @param   cell_y          The cell y coordinate.
     * @param   spac_x          The spac x coordinate.
     * @param   spac_y          The spac y coordinate.
     * @param   startCorner     The start corner.
     * @param   startAxis       The start axis.
     * @param   childAlignment  The child alignment.
     * @param   constraint      The constraint.
     */

    public void addGridLayout(
        GameObject contentArea,
        int left, int right,
        int top, int bottom,
        float cell_x, float cell_y,
        float spac_x, float spac_y,
        GridLayoutGroup.Corner startCorner,
        GridLayoutGroup.Axis startAxis,
        TextAnchor childAlignment,
        GridLayoutGroup.Constraint constraint
        )
    {
        // Add Grid Layout
        GridLayoutGroup glg = contentArea.AddComponent<GridLayoutGroup>();
        RectOffset ro = glg.padding;
        ro.left = left;
        ro.right = right;
        ro.top = top;
        ro.bottom = bottom;
        glg.cellSize = new Vector2(cell_x, cell_y);
        glg.spacing = new Vector2(spac_x, spac_y);
        glg.startCorner = startCorner;
        glg.startAxis = startAxis;
        glg.childAlignment = childAlignment;
        glg.constraint = constraint;
    }

    /**
     * @fn  public void addScrollBar( GameObject panel, Scrollbar.Direction direction )
     *
     * @brief   Horizontal NYI for createPanel.
     *
     * @param   panel       The panel.
     * @param   direction   The direction.
     */

    public void addScrollBar(
        GameObject panel,
        Scrollbar.Direction direction
        )
    {
        float scrollBarWidth = 15f;
        float scrollSliderW = 10f;
        float scrollSliderH = 10f;
        float scrollOffsetX = -5f;
        float scrollOffsetY = -18f;
        float handleSize = 0.2f; //0.0-1.0f
        float hoverAlpha = 1f; // Alpha on hover
        float normalAlpha = 0.2f; // Normal Alpha

        GameObject scrollBarObj = new GameObject();
        scrollBarObj.transform.SetParent(panel.transform);
        scrollBarObj.name = panel.name + "ScrollBar";
        RectTransform sbRT = scrollBarObj.AddComponent<RectTransform>();
        GameObject maskAreaObj = GameObject.Find(panel.name + "MaskScrollArea");
        RectTransform maRT = maskAreaObj.GetComponent<RectTransform>();
        // set W and H of scroll bar
        if (direction == Scrollbar.Direction.BottomToTop || direction == Scrollbar.Direction.TopToBottom)
        {
            sbRT.sizeDelta = new Vector2(scrollBarWidth, maRT.rect.height);
        } else
        {
            sbRT.sizeDelta = new Vector2(maRT.rect.width, scrollBarWidth);
        }
        // Add Image
        Image img = scrollBarObj.AddComponent<Image>();
        img.type = Image.Type.Sliced;
        img.sprite = scrollBarImage;
        // Add Scrollbar
        Scrollbar sb = scrollBarObj.AddComponent<Scrollbar>();
        sb.SetDirection(direction, false);
        ScrollRect sr = maskAreaObj.GetComponentInChildren<ScrollRect>();
        sr.verticalScrollbar = sb; //horizontal NYI
        // Add ScrollBar hover handle
        ScrollHandleHover shh = scrollBarObj.AddComponent<ScrollHandleHover>();
        shh.hoverAlpha = hoverAlpha;
        shh.normalAlpha = normalAlpha;
        // Position scroll bar obj
        float minX, minY, maxX, maxY, pivotX, pivotY;
        if(direction == Scrollbar.Direction.BottomToTop || direction == Scrollbar.Direction.TopToBottom)
        {
            minX = maxX = pivotX = 1f;
            minY = maxY = pivotY = 0.5f;
        } else
        {
            minX = maxX = pivotX = 0.5f;
            minY = maxY = pivotY = 0f;
        }
        // Set anchor
        sbRT.anchorMin = new Vector2(minX, minY);
        sbRT.anchorMax = new Vector2(maxX, maxY);
        sbRT.pivot = new Vector2(pivotX, pivotY);
        // Set Anchored Position
        sbRT.anchoredPosition = new Vector2(scrollOffsetX, scrollOffsetY);
        // Add Sroll Bar Sliding Area
        GameObject slidingAreaObj = new GameObject();
        slidingAreaObj.name = "Sliding Area";
        slidingAreaObj.transform.SetParent(scrollBarObj.transform);
        // Add RectTransform with Stretch
        RectTransform saRT = slidingAreaObj.AddComponent<RectTransform>();
        // Set stretch
        saRT.anchorMin = new Vector2(0f, 0f);
        saRT.anchorMax = new Vector2(1f, 1f);
        saRT.pivot = new Vector2(0.5f, 0.5f);
        saRT.anchoredPosition = new Vector2(0, 0);
        if (direction == Scrollbar.Direction.BottomToTop || direction == Scrollbar.Direction.TopToBottom)
        {
            saRT.sizeDelta = new Vector2(scrollSliderW, scrollSliderH);
        } else
        {
            saRT.sizeDelta = new Vector2(scrollSliderH, scrollSliderW);
        }
        // Add Handle object
        GameObject handle = new GameObject();
        handle.name = "Handle";
        handle.transform.SetParent(slidingAreaObj.transform);
        // Add RectTransform
        RectTransform handleRT = handle.AddComponent<RectTransform>();
        if (direction == Scrollbar.Direction.BottomToTop || direction == Scrollbar.Direction.TopToBottom)
        {
            handleRT.sizeDelta = new Vector2(0-scrollSliderW, 0-scrollSliderH);
            handleRT.anchorMin = new Vector2(0f, 0.5f);
            handleRT.anchorMax = new Vector2(1f, 0.5f);
        }
        else
        {
            handleRT.sizeDelta = new Vector2(0-scrollSliderH, 0-scrollSliderW);
            handleRT.anchorMin = new Vector2(0.5f, 0f);
            handleRT.anchorMax = new Vector2(0.5f, 1f);
        }
        handleRT.pivot = new Vector2(0.5f, 0.5f);
        handleRT.anchoredPosition = new Vector2(0, 0);
        // Add Image to Scrollbar handle
        Image handleImg = handle.AddComponent<Image>();
        handleImg.fillCenter = true;
        handleImg.sprite = scrollBarHandleImage;
        handleImg.type = Image.Type.Sliced;
        // Add Handle Hover
        ScrollHandleHover shhh = handle.AddComponent<ScrollHandleHover>();
        shhh.hoverAlpha = hoverAlpha;
        shhh.normalAlpha = normalAlpha;
        // Add Rect to Scrollbar handle
        sb.handleRect = handleRT;
        sb.size = handleSize;
    }

    /**
     * @fn  public GameObject createScrollPanel( string objectName, string windowName, float off_x, float off_y, float w, float h, bool draggable)
     *
     * @brief   Creates scroll panel.
     *
     * @param   objectName  Name of the object.
     * @param   windowName  Name of the window.
     * @param   off_x       The off x coordinate.
     * @param   off_y       The off y coordinate.
     * @param   w           The width.
     * @param   h           The height.
     * @param   draggable   True if draggable.
     *
     * @return  The new scroll panel.
     */

    public GameObject createScrollPanel(
        string objectName, string windowName,
        float off_x, float off_y,
        float w, float h,
        bool draggable)
    {
        GameObject panel = createPanel(objectName, windowName, off_x, off_y, w, h, draggable);
        addScrollBar(panel, Scrollbar.Direction.BottomToTop);

        return panel;
    }

    /**
     * @fn  public GameObject getContentAreaGameObject(GameObject panel)
     *
     * @brief   Gets content area game object.
     *
     * @param   panel   The panel.
     *
     * @return  The content area game object.
     */

    public GameObject getContentAreaGameObject(GameObject panel)
    {
        return panel.transform.GetChild(2).GetChild(0).gameObject;
    }

    /**
     * @fn  public GameObject createInputField(string fieldName, string placeHolderText, string textValue, float ifW, float ifH)
     *
     * @brief   Creates input field.
     *
     * @param   fieldName       Name of the field.
     * @param   placeHolderText The place holder text.
     * @param   textValue       The text value.
     * @param   ifW             if w.
     * @param   ifH             if h.
     *
     * @return  The new input field.
     */

    public GameObject createInputField(string fieldName, string placeHolderText, string textValue, float ifW, float ifH)
    {

        Vector2 fieldPadding = new Vector2(0-18, 0-14);

        GameObject inputFieldGO = new GameObject();
        inputFieldGO.name = fieldName;
        RectTransform ifRT = inputFieldGO.AddComponent<RectTransform>();
        ifRT.sizeDelta = new Vector2(ifW, ifH);
        Image ifImg = inputFieldGO.AddComponent<Image>();
        ifImg.sprite = inputFieldBackground;
        ifImg.type = Image.Type.Sliced;
        ifImg.fillCenter = true;
        // Create Child Objects
        Vector2 anchorMin = new Vector2(0f, 0f);
        Vector2 anchorMax = new Vector2(1f, 1f);
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        Vector2 sizeDelta = fieldPadding;
        // Placeholder object
        GameObject placeholderGO = new GameObject();
        placeholderGO.name = "Placeholder";
        placeholderGO.transform.SetParent(inputFieldGO.transform);
        RectTransform pRT = placeholderGO.AddComponent<RectTransform>();
        pRT.anchorMin = anchorMin;
        pRT.anchorMax = anchorMax;
        pRT.pivot = pivot;
        pRT.sizeDelta = sizeDelta;
        Text pText = placeholderGO.AddComponent<Text>();
        pText.color = Color.black;
        pText.font = textFont;
        pText.text = placeHolderText;
        pText.resizeTextForBestFit = true;
        if (!string.IsNullOrEmpty(textValue))
            pText.enabled = false;
        // Text object
        GameObject textGO = new GameObject();
        textGO.name = "Text";
        textGO.transform.SetParent(inputFieldGO.transform);
        RectTransform tRT = textGO.AddComponent<RectTransform>();
        tRT.anchorMin = anchorMin;
        tRT.anchorMax = anchorMax;
        tRT.pivot = pivot;
        tRT.sizeDelta = sizeDelta;
        Text tText = textGO.AddComponent<Text>();
        tText.color = Color.black;
        tText.font = textFont;
        tText.supportRichText = false;
        tText.resizeTextForBestFit = true;
        // Allocated child objects to Input Field GO
        InputField inputField = inputFieldGO.AddComponent<InputField>();
        inputField.targetGraphic = ifImg;
        inputField.textComponent = tText;
        inputField.placeholder = pText;
        inputField.interactable = true;
        inputField.text = textValue;
        return inputFieldGO;
    }


    public delegate bool SNCCancelCallBack();
    public delegate bool SNCApplyCallBack(float value);

    /**
     * @fn  public GameObject addSimpleNumberCounter( GameObject rootPanel, GameObject contentArea, string valueDesc, float currentValue, float minValue, float maxValue, SNCApplyCallBack applyCallback, bool showCur)
     *
     * @brief   Adds a simple number counter.
     *
     * @param   rootPanel       The root panel.
     * @param   contentArea     The content area.
     * @param   valueDesc       Information describing the value.
     * @param   currentValue    The current value.
     * @param   minValue        The minimum value.
     * @param   maxValue        The maximum value.
     * @param   applyCallback   The apply callback.
     * @param   showCur         True to show, false to hide the current.
     *
     * @return  A GameObject.
     */

    public GameObject addSimpleNumberCounter(
        GameObject rootPanel,
        GameObject contentArea, string valueDesc,
        float currentValue, float minValue, 
        float maxValue, SNCApplyCallBack applyCallback,
        bool showCur)
    {

        string inputFieldName = "valueInputField";
        string placeholder = "Enter value between: " + minValue + " and " + maxValue;
        if (maxValue == 0)
        {
            placeholder = "Nothing to add";
        }
        string textValue = currentValue.ToString();
        if (!showCur)
            textValue = "";
        string applyString = "Apply";
        string cancelString = "Cancel";
        Vector2 labelAnchorPos = new Vector2(0, 0);
        Vector2 labelSize = new Vector2(236, 30);
        Vector2 ifSize = new Vector2(236, 30);
        Vector2 ifAnchorPos = new Vector2(0, -21);
        Vector2 buttonSize = new Vector2(ifSize.x/2, 30);
        Vector2 applyButtonPos = new Vector2(0 - (buttonSize.x / 2), -57f);
        Vector2 cancelButtonPos = new Vector2((buttonSize.x / 2), -57f);
        Vector2 errorTextAnchorPos = new Vector2(0,  0-88);

        // Add label
        GameObject labelGO = new GameObject();
        labelGO.name = "label";
        labelGO.transform.SetParent(contentArea.transform);
        RectTransform lRT = labelGO.AddComponent<RectTransform>();
        lRT.anchorMin = anchorTopCenterMin;
        lRT.anchorMax = anchorTopCenterMax;
        lRT.pivot = anchorTopCenterPivot;
        lRT.sizeDelta = labelSize;
        lRT.anchoredPosition = labelAnchorPos;
        Text labelText = labelGO.AddComponent<Text>();
        labelText.color = Color.black;
        labelText.font = textFont;
        labelText.text = valueDesc;
        labelText.alignment = TextAnchor.UpperCenter;
        // Create Input Field
        GameObject inputField = createInputField(inputFieldName, placeholder, textValue, ifSize.x, ifSize.y);
        inputField.transform.SetParent(contentArea.transform);
        // Set input field anchor and position
        RectTransform ifRT = inputField.GetComponent<RectTransform>();
        ifRT.anchorMin = anchorTopCenterMin;
        ifRT.anchorMax = anchorTopCenterMax;
        ifRT.pivot = anchorTopCenterPivot;
        ifRT.sizeDelta = ifSize;
        ifRT.anchoredPosition = ifAnchorPos;
        // Add Apply and Cancel buttons
        GameObject applyButton = new GameObject();
        applyButton.name = "ApplyButton";
        applyButton.transform.SetParent(contentArea.transform);
        RectTransform abRT = applyButton.AddComponent<RectTransform>();
        abRT.sizeDelta = buttonSize;
        abRT.anchorMin = anchorTopCenterMin;
        abRT.anchorMax = anchorTopCenterMax;
        abRT.pivot = anchorTopCenterPivot;
        abRT.anchoredPosition = applyButtonPos;
        Image applyImg = applyButton.AddComponent<Image>();
        applyImg.sprite = buttonImage;
        applyImg.type = Image.Type.Sliced;
        Button applyBtn = applyButton.AddComponent<Button>();
        applyBtn.targetGraphic = applyImg;
        // Add Text under apply button
        GameObject applyButtonText = new GameObject();
        applyButtonText.name = "CancelButtonText";
        applyButtonText.transform.SetParent(applyButton.transform);
        RectTransform abtRT = applyButtonText.AddComponent<RectTransform>();
        abtRT.anchorMin = anchorStretchAllMin;
        abtRT.anchorMax = anchorStretchAllMax;
        abtRT.pivot = anchorStretchAllPivot;
        abtRT.anchoredPosition = new Vector2(0, 0);
        abtRT.sizeDelta = new Vector2(0, 0);
        Text applyTxt = applyButtonText.AddComponent<Text>();
        applyTxt.color = Color.black;
        applyTxt.font = textFont;
        applyTxt.text = applyString;
        applyTxt.alignment = TextAnchor.MiddleCenter;
        // Add Cancel button
        GameObject cancelButton = new GameObject();
        cancelButton.name = "CancelButton";
        cancelButton.transform.SetParent(contentArea.transform);
        RectTransform cbRT = cancelButton.AddComponent<RectTransform>();
        cbRT.sizeDelta = buttonSize;
        cbRT.anchorMin = anchorTopCenterMin;
        cbRT.anchorMax = anchorTopCenterMax;
        cbRT.pivot = anchorTopCenterPivot;
        cbRT.anchoredPosition = cancelButtonPos;
        Image cancelImg = cancelButton.AddComponent<Image>();
        cancelImg.sprite = buttonImage;
        cancelImg.type = Image.Type.Sliced;
        Button cancelBtn = cancelButton.AddComponent<Button>();
        cancelBtn.targetGraphic = cancelImg;
        // Add Text under cancel button
        GameObject cancelButtonText = new GameObject();
        cancelButtonText.name = "CancelButtonText";
        cancelButtonText.transform.SetParent(cancelButton.transform);
        RectTransform cbtRT = cancelButtonText.AddComponent<RectTransform>();
        cbtRT.anchorMin = anchorStretchAllMin;
        cbtRT.anchorMax = anchorStretchAllMax;
        cbtRT.pivot = anchorStretchAllPivot;
        cbtRT.anchoredPosition = new Vector2(0, 0);
        cbtRT.sizeDelta = new Vector2(0, 0);
        Text cancelTxt = cancelButtonText.AddComponent<Text>();
        cancelTxt.color = Color.black;
        cancelTxt.font = textFont;
        cancelTxt.text = cancelString;
        cancelTxt.alignment = TextAnchor.MiddleCenter;
        // Add Error Checking Text
        GameObject errorTextGO = new GameObject();
        errorTextGO.name = "ErrorText";
        errorTextGO.transform.SetParent(contentArea.transform);
        RectTransform etRT = errorTextGO.AddComponent<RectTransform>();
        etRT.anchorMin = anchorTopCenterMin;
        etRT.anchorMax = anchorTopCenterMax;
        etRT.pivot = anchorTopCenterPivot;
        etRT.sizeDelta = ifRT.sizeDelta;
        etRT.anchoredPosition = errorTextAnchorPos;
        Text etTxt = errorTextGO.AddComponent<Text>();
        etTxt.font = textFont;
        etTxt.color = Color.red;
        etTxt.alignment = TextAnchor.MiddleCenter;
        etTxt.enabled = false;



        // Add callbacks to buttons
        applyBtn.onClick.AddListener(() => 
        {
            string errorEmptyException = "No number provided";
            string errorFormatException = "Invalid number format";
            string errorOverflowException = "Number out of bounds";
            string value = inputField.GetComponent<InputField>().text;
            if (string.IsNullOrEmpty(value))
            {
                etTxt.text = errorEmptyException;
                etTxt.enabled = true;
            }
            else
            {
                float fValue;
                try
                {
                    fValue = float.Parse(value, CultureInfo.InvariantCulture); // float format set as 0.0
                    if (fValue < minValue || fValue > maxValue)
                    {
                        etTxt.text = errorOverflowException;
                        etTxt.enabled = true;
                    }
                    else
                    {
                        applyCallback(fValue);
                        Destroy(rootPanel);
                    }
                }
                catch (System.FormatException)
                {
                    etTxt.text = errorFormatException;
                    etTxt.enabled = true;
                }
            }
            
        });
        cancelBtn.onClick.AddListener(() => 
        {
            Destroy(rootPanel);
        });

        return contentArea;
    }

    /**
     * @fn  public GameObject createSimpleNumberCounterPanel( string windowName, string valueDesc, float currentValue, float minValue, float maxValue, SNCApplyCallBack applyCallback, bool showCur)
     *
     * @brief   Creates simple number counter panel.
     *
     * @param   windowName      Name of the window.
     * @param   valueDesc       Information describing the value.
     * @param   currentValue    The current value.
     * @param   minValue        The minimum value.
     * @param   maxValue        The maximum value.
     * @param   applyCallback   The apply callback.
     * @param   showCur         True to show, false to hide the current.
     *
     * @return  The new simple number counter panel.
     */

    public GameObject createSimpleNumberCounterPanel(
        string windowName,
        string valueDesc, float currentValue,
        float minValue, float maxValue,
        SNCApplyCallBack applyCallback,
        bool showCur)
    {
        float defaultW = 300f;
        float defaultH = 175f;
        GameObject panel = createPanel(windowName, windowName, 0, 0, defaultW, defaultH, true);
        GameObject contentArea = Utils.getChildGameObject(panel, windowName + "PanelContentArea");
        panel.tag = "InputPopup";
        addSimpleNumberCounter(panel, contentArea, valueDesc, currentValue, minValue, maxValue, applyCallback, showCur);
        return panel;
    }

    /**
     * @fn  void destoryPanel(GameObject panel)
     *
     * @brief   Destory panel.
     *
     * @param   panel   The panel.
     */

    void destoryPanel(GameObject panel)
    {
        Destroy(panel);
    }

    /**
     * @fn  public GameObject createEquipmentMenu(GameObject obj)
     *
     * @brief   Creates equipment menu.
     *
     * @param   obj The object.
     *
     * @return  The new equipment menu.
     */

    public GameObject createEquipmentMenu(GameObject obj)
    {
        
        float menuWidth = 100;
        float menuHeight = menuWidth / 2;
        float buttonWidth = 50;
        float buttonHeight = buttonWidth;
        float iconWidth = 40;
        float iconHeight = iconWidth;
        GameObject equipmentMenuGO = new GameObject();
        equipmentMenuGO.transform.SetParent(getCanvas().transform);
        RectTransform rmRT = equipmentMenuGO.AddComponent<RectTransform>();
        rmRT.sizeDelta = new Vector2(menuWidth, menuHeight);
        rmRT.position = getMouseScreenPos(obj);
        // Add Config Button
        GameObject configButtonGO = createButtonWithIcon(buttonImage, buttonWidth, buttonHeight, configIcon, iconWidth, iconHeight);
        GameObject trashButtonGO = createButtonWithIcon(buttonImage, buttonWidth, buttonHeight, trashIcon, iconWidth, iconHeight);
        // ConfigButton
        RectTransform cBtnRT = configButtonGO.GetComponent<RectTransform>();
        cBtnRT.transform.SetParent(equipmentMenuGO.transform);
        cBtnRT.anchorMin = anchorMiddleLeftMin;
        cBtnRT.anchorMax = anchorMiddleLeftMax;
        cBtnRT.pivot = anchorMiddleLeftPivot;
        cBtnRT.anchoredPosition = new Vector2(0, 0);
        // TrashButton
        RectTransform tBtnRT = trashButtonGO.GetComponent<RectTransform>();
        tBtnRT.transform.SetParent(equipmentMenuGO.transform);
        tBtnRT.anchorMin = anchorMiddleRightMin;
        tBtnRT.anchorMax = anchorMiddleRightMax;
        tBtnRT.pivot = anchorMiddleRightPivot;
        tBtnRT.anchoredPosition = new Vector2(0, 0);
        // Add listeners
        Button configBtn = configButtonGO.GetComponent<Button>();
        Button trashBtn = trashButtonGO.GetComponent<Button>();
        configBtn.onClick.AddListener(() => 
        {
            Equipment eqp = obj.GetComponent<Equipment>();
            eqp.showConfigureMenu();
            Destroy(equipmentMenuGO);
        });
        trashBtn.onClick.AddListener(() =>
        {
            Debug.Log("Destroying");
            Destroy(obj);
            Destroy(equipmentMenuGO);
        });




        return equipmentMenuGO;
    }

    /**
     * @fn  public GameObject createButtonWithIcon(Sprite buttonImage, float bWidth, float bHeight, Sprite buttonIcon, float iWidth, float iHeight)
     *
     * @brief   Creates button with icon.
     *
     * @param   buttonImage The button image.
     * @param   bWidth      The width.
     * @param   bHeight     The height.
     * @param   buttonIcon  The button icon.
     * @param   iWidth      Zero-based index of the width.
     * @param   iHeight     Zero-based index of the height.
     *
     * @return  The new button with icon.
     */

    public GameObject createButtonWithIcon(Sprite buttonImage, float bWidth, float bHeight, Sprite buttonIcon, float iWidth, float iHeight)
    {
        GameObject buttonGO = new GameObject();
        buttonGO.name = "Button";
        RectTransform bRT = buttonGO.AddComponent<RectTransform>();
        bRT.sizeDelta = new Vector2(bWidth, bHeight);
        Image bImage = buttonGO.AddComponent<Image>();
        bImage.sprite = buttonImage;
        GameObject iconGO = new GameObject();
        iconGO.name = "Icon";
        iconGO.transform.SetParent(buttonGO.transform);
        RectTransform iRT = iconGO.AddComponent<RectTransform>();
        iRT.anchorMin = anchorMiddleCenterMin;
        iRT.anchorMax = anchorMiddleCenterMax;
        iRT.pivot = anchorMiddleCenterPivot;
        iRT.sizeDelta = new Vector2(iWidth, iHeight);
        iRT.position = new Vector2(0, 0);
        Image iImg = iconGO.AddComponent<Image>();
        iImg.sprite = buttonIcon;
        iImg.type = Image.Type.Sliced;
        // button
        Button btn = buttonGO.AddComponent<Button>();
        btn.targetGraphic = bImage;


        return buttonGO;
    }

    /**
     * @fn  public Vector3 getMouseScreenPos(GameObject obj)
     *
     * @brief   Gets mouse screen position.
     *
     * @param   obj The object.
     *
     * @return  The mouse screen position.
     */

    public Vector3 getMouseScreenPos(GameObject obj)
    {
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(obj.transform.position);
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        return cursorPoint;

    }

    /**
     * @fn  public static GameObject getCanvas()
     *
     * @brief   Gets the canvas.
     *
     * @return  The canvas.
     */

    public static GameObject getCanvas()
    {
        return GameObject.Find("Canvas");
    }

    /**
     * @fn  public static UIController getUIController()
     *
     * @brief   Gets user interface controller.
     *
     * @return  The user interface controller.
     */

    public static UIController getUIController()
    {
        return GameObject.Find("Utils").GetComponent<UIController>();
    }

    /**
     * @fn  public GameObject createTooltip()
     *
     * @brief   Creates the tooltip.
     *
     * @return  The new tooltip.
     */

    public GameObject createTooltip()
    {
        GameObject tooltip = new GameObject();
        tooltip.name = "tooltip";
        tooltip.transform.SetParent(UIController.getCanvas().transform);
        RectTransform tooltipRT = tooltip.AddComponent<RectTransform>();
        Image img = tooltip.AddComponent<Image>();
        img.sprite = this.tooltipBackground;
        img.type = Image.Type.Sliced; 
        VerticalLayoutGroup vlg = tooltip.AddComponent<VerticalLayoutGroup>();
        vlg.childAlignment = TextAnchor.MiddleCenter;
        vlg.childForceExpandHeight = true;
        vlg.childForceExpandWidth = true;
        vlg.padding = new RectOffset(10, 10, 10, 10);
        ContentSizeFitter csf = tooltip.AddComponent<ContentSizeFitter>();
        csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;


        tooltipRT.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);

        return tooltip;
    }

    /**
     * @fn  public void createTimer()
     *
     * @brief   Creates the timer.
     */

    public void createTimer()
    {
        GameObject placeHolderTimer = GameObject.Find("TimerPlaceholder");
        if (placeHolderTimer == null)
            return;
        GameObject timerGO = Instantiate(placeHolderTimer) as GameObject;
        timerGO.name = "Timer";
        timerGO.transform.SetParent(UIController.getCanvas().transform, false);
        RectTransform timerRT = timerGO.GetComponent<RectTransform>();
        timerRT.anchorMin = anchorMiddleCenterMin;
        timerRT.anchorMax = anchorMiddleCenterMax;
        timerRT.pivot = anchorMiddleCenterPivot;
        timerRT.localPosition = Vector2.zero;
        Timer timer = timerGO.GetComponent<Timer>();
        timer.SetLabel("T(" + timerCount + ")");
        timerCount++;
        
        
    }

    /**
     * @fn  public void createLabManual()
     *
     * @brief   Creates lab manual.
     */

    public void createLabManual()
    {
        if (labManual != null)
            return;
        labManual = createScrollPanel("Lab Manual", "Lab Manual", 0, 0, 600, 600, true);
        GameObject contentArea = getContentAreaGameObject(labManual);
        VerticalLayoutGroup vlg = contentArea.AddComponent<VerticalLayoutGroup>();
        vlg.spacing = 8;
        float defHeight = 50f;

        List<ManualElement> manualElements = LabManager.getLabManagerGO().GetComponent<LabManager>().getManaulElements();
        foreach (ManualElement manualElement in manualElements)
        {
            if(manualElement is TextElement)
            {
                TextElement te = manualElement as TextElement;
                bool isUnderline = te.isUnderline();
                bool isBold = te.isBold();
                bool isHeading = te.isH1();
                addTextToLabManual(labManual, contentArea.transform, te.getText(), isHeading, isBold, isUnderline);
            } else if(manualElement is ImageElement)
            {
                ImageElement ie = manualElement as ImageElement;
                TextAlignment ta = TextAlignment.Center; //default to Center
                if (ie.isLeft())
                    ta = TextAlignment.Left;
                if (ie.isRight())
                    ta = TextAlignment.Right;
                Sprite sprite = Resources.Load<Sprite>(ie.getImagePath());
                Debug.Log("img: " + ie.getImagePath() + ", height: " + ie.height);
                float spriteHeight = defHeight;
                if (ie.height > 0)
                    spriteHeight = ie.height;
                addImagetoLabManual(labManual, sprite, spriteHeight, true, Image.Type.Simple, ta, true);
            } else if(manualElement is ListElement)
            {
                int indentLeft = 45;
                int indentRight = 20;
                int indentTop = 10;
                int indentBottom = 10;

                ListElement le = manualElement as ListElement;

                GameObject textGO = addTextToLabManual(labManual, contentArea.transform, "", false, false, false); //Create empty text to use an indent
                VerticalLayoutGroup textVlg = textGO.AddComponent<VerticalLayoutGroup>();
                textVlg.childForceExpandWidth = false;
                textVlg.padding = new RectOffset(indentLeft, indentRight, indentTop, indentBottom);
                textGO.name = "List";
                int count = 0;
                foreach(TextElement te in le.getChildren())
                {
                    bool isUnderline = te.isUnderline();
                    bool isBold = te.isBold();
                    bool isHeading = te.isH1();
                    string prefix = "";
                    if (le.isAlpha())
                        prefix = ((char) ('A' + count)) + ") ";
                    if (le.isNumber())
                        prefix = (count+1) + ") ";
                    if (le.isBullet())
                        prefix = "• ";
                    GameObject indentTextGO = addTextToLabManual(textGO, textGO.transform, prefix + te.getText(), isHeading, isBold, isUnderline);
                    Destroy(indentTextGO.GetComponent<LayoutElement>()); // Remove LayoutElement that was added by addTextToLabManual
                    count++;
                }
                
                


            }


        }
        
    }

    /**
     * @fn  public GameObject addTextToLabManual(GameObject labManual, Transform parent, string content, bool isHeading, bool isBold, bool isUnderline)
     *
     * @brief   Adds a text to lab manual.
     *
     * @param   labManual   The lab manual.
     * @param   parent      The parent.
     * @param   content     The content.
     * @param   isHeading   True if this object is heading.
     * @param   isBold      True if this object is bold.
     * @param   isUnderline True if this object is underline.
     *
     * @return  A GameObject.
     */

    public GameObject addTextToLabManual(GameObject labManual, Transform parent, string content, bool isHeading, bool isBold, bool isUnderline)
    {
        int headerFontSizeIncrement = 4;

        GameObject textGO = new GameObject();
        textGO.transform.SetParent(parent);
        textGO.name = "Text";
        Text text = textGO.AddComponent<Text>();
        text.text = content;
        text.font = textFont;
        text.color = Color.black;
        text.fontStyle = FontStyle.Normal;
        if (isHeading)
            text.fontSize = text.fontSize + headerFontSizeIncrement;
        if (isBold)
            text.fontStyle = FontStyle.Bold;
        LayoutElement le = textGO.AddComponent<LayoutElement>();
        if (isUnderline)
        {
            GameObject underlineGO = GameObject.Instantiate(textGO, textGO.transform.position, textGO.transform.rotation, textGO.transform) as GameObject;
            Text underlineText = underlineGO.GetComponent<Text>();
            string underlineStr = "";
            underlineText.text = underlineStr;
            float prefWidth = text.preferredWidth;
            while (underlineText.preferredWidth < prefWidth)
            {
                underlineStr += "_";
                underlineText.text = underlineStr;
            }
            RectTransform underlineRT = underlineGO.GetComponent<RectTransform>();
            underlineRT.anchorMin = anchorStretchAllMin;
            underlineRT.anchorMax = anchorStretchAllMax;
            underlineRT.pivot = anchorStretchAllPivot;
            underlineRT.anchoredPosition = Vector2.zero;
            underlineRT.offsetMin = new Vector2(0, 0);
            underlineRT.offsetMax = new Vector2(0, 0);
        }


        return textGO;
    }

    /**
     * @fn  public void addImagetoLabManual(GameObject labManual, Sprite sprite, float height, bool preserveAspect, Image.Type imageType, TextAlignment imageAlignment, bool fillCenter)
     *
     * @brief   Adds an imageto lab manual.
     *
     * @param   labManual       The lab manual.
     * @param   sprite          The sprite.
     * @param   height          The height.
     * @param   preserveAspect  True to preserve aspect.
     * @param   imageType       Type of the image.
     * @param   imageAlignment  The image alignment.
     * @param   fillCenter      True to fill center.
     */

    public void addImagetoLabManual(GameObject labManual, Sprite sprite, float height, bool preserveAspect, Image.Type imageType, TextAlignment imageAlignment, bool fillCenter)
    {
        GameObject imageContainerGO = new GameObject();
        imageContainerGO.transform.SetParent(getContentAreaGameObject(labManual).transform);
        imageContainerGO.name = "Image Container";
        LayoutElement le = imageContainerGO.AddComponent<LayoutElement>();
        le.minWidth = labManual.GetComponent<RectTransform>().sizeDelta.x;
        HorizontalLayoutGroup hlg = imageContainerGO.AddComponent<HorizontalLayoutGroup>();
        hlg.childAlignment = TextAnchor.MiddleCenter;
        hlg.childForceExpandHeight = false;
        hlg.childForceExpandWidth = false;
        GameObject imageGO = new GameObject();
        imageGO.transform.SetParent(imageContainerGO.transform);
        imageGO.name = "Image";
        Image image = imageGO.AddComponent<Image>();
        image.sprite = sprite;
        image.preserveAspect = preserveAspect;
        image.type = imageType;
        image.fillCenter = fillCenter;
        LayoutElement imageLE = imageGO.AddComponent<LayoutElement>();
        // imageLE.preferredWidth = width;
        imageLE.preferredHeight = height;
    }



}
