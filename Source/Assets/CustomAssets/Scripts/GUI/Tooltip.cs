﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Tooltip : MonoBehaviour {

    GameObject tooltip;
    List<string> text;
    UIController uiController;

    Container container;

    /**
     * @fn  void Awake()
     *
     * @brief   Awakes this object.
     */

    void Awake()
    {
        clearTooltip();
    }

    /**
     * @fn  void Start()
     *
     * @brief   Starts this object.
     */

    void Start()
    {
        uiController = UIController.getUIController();
        container = gameObject.GetComponent<Container>();
    }

    /**
     * @fn  public void addToTooltip(string text)
     *
     * @brief   Adds to the tooltip.
     *
     * @param   text    The text.
     */

	public void addToTooltip(string text)
    {
        this.text.Add(text);
    }

    /**
     * @fn  public void clearTooltip()
     *
     * @brief   Clears the tooltip.
     */

    public void clearTooltip()
    {
        text = new List<string>();
    }

    /**
     * @fn  void Update()
     *
     * @brief   Updates this object.
     */

    void Update()
    {
        if (tooltip != null)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Destroy(tooltip);
                return; // Prevent hover through UI
            }
            float yOffest = tooltip.GetComponent<RectTransform>().sizeDelta.y;
            tooltip.transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y + yOffest);
        }
    }

    /**
     * @fn  void OnMouseOver()
     *
     * @brief   Executes the mouse over action.
     */

    void OnMouseOver()
    {
        if (EventSystem.current.IsPointerOverGameObject())        
                return; // Prevent hover through UI        
        if (Input.GetMouseButton(0))
        {
            if (tooltip != null)
                Destroy(tooltip);
            return;
        }
            
        if (text.Count <= 0)
            return;
        if (tooltip != null)
            return;

        tooltip = uiController.createTooltip();

        List<string> textCopy = new List<string>(text);

        

        if(container != null)
        {
            textCopy.Add(string.Format("\"{0}\"", container.gameObject.GetComponent<Equipment>().itemName));
            string measurement = container.getMeasurement();
            float current = container.getCurrent();
            textCopy.Add(string.Format("Volume({0}): {1}", measurement, current));
        }

        foreach (string txt in textCopy)
        {
            GameObject tip = new GameObject();
            tip.transform.SetParent(tooltip.transform);
            Text tipText = tip.AddComponent<Text>();
            tipText.font = uiController.textFont;
            tipText.alignment = TextAnchor.MiddleCenter;
            tipText.text = txt;
            tipText.color = Color.black;
        }
    }

    /**
     * @fn  void OnMouseExit()
     *
     * @brief   Executes the mouse exit action.
     */

    void OnMouseExit()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return; // Prevent exit through UI
        if (tooltip != null)
            Destroy(tooltip);
    }

}
