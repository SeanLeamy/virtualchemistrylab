﻿using UnityEngine;
using System.Collections;

public class DynamicViewport : MonoBehaviour {

    public GameObject contentToSpan;
    RectTransform rt;

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

    void Start () {
	    rt = gameObject.GetComponent<RectTransform>();
    }

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

	void Update () {
        RectTransform rtToSpan = contentToSpan.GetComponent<RectTransform>();
        rt.sizeDelta = rtToSpan.sizeDelta;
	}
}
