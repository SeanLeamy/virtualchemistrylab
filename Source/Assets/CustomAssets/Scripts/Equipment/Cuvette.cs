﻿using UnityEngine;
using System.Collections;
using System;

public class Cuvette : Equipment {

    private Sprite fillerSprite;

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {
        
    }

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {
        panelTitle = "Cuvette";
        itemName = "Cuvette";
        spritePath = "Sprites/Equipment/cuvette";
        equipmentSprite = Resources.Load<Sprite>(spritePath);
        spriteScale = 0.3f;
        string fillerSpritePath = "Sprites/Equipment/cuvetteFiller";
        fillerSprite = Resources.Load<Sprite>(fillerSpritePath);
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {
        equipmentName = string.Format("Cuvette {0} {1}", this.containerMax, this.containerMeausrement);
        panelDesc = string.Format("Cuvette ({0})", this.containerMeausrement);
    }

    /**
     * @fn  void Update()
     *
     * @brief   Update is called once per frame.
     */

    void Update()
    {
        
    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<Draggable>();
        equipmentGameObject.AddComponent<DraggableContainer>();
        Container container = equipmentGameObject.AddComponent<Container>();
        container.setMin(this.containerMin);
        container.setMax(this.containerMax);
        container.setMeasurement(this.containerMeausrement);
        container.initFillerSprite(fillerSprite);
        Tooltip tooltip = equipmentGameObject.AddComponent<Tooltip>();
        tooltip.addToTooltip(equipmentName);
    }

}
