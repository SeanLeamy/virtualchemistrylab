﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Equipment : MonoBehaviour {


    // Equipment 
    public string spritePath;
    public Sprite equipmentSprite;
    public float spriteScale;
    public string equipmentName;
    public string name;
    public string itemName;
    // Settings UI
    protected UIController uiController;
    protected GameObject uiPanel;
    protected GameObject uiContentArea;
    protected string panelDesc;
    protected string panelTitle;
    protected float panelSizeW;
    protected float panelSizeH;
    // Container Specs
    protected bool isContainer = false;
    protected float containerMax;
    protected float containerMin = 0;
    protected string containerMeausrement;

    /**
     * @fn  public Sprite getEquipmentSprite()
     *
     * @brief   Gets equipment sprite.
     *
     * @return  The equipment sprite.
     */

    public Sprite getEquipmentSprite()
    {
        return equipmentSprite;
    }

    /**
     * @fn  void Awake()
     *
     * @brief   Awakes this object.
     */

    void Awake()
    {
        initStaticMembers();
        if (uiController == null)
        {
            uiController = GameObject.Find("Utils").GetComponent<UIController>();
        }
    }

    /**
     * @fn  void Start()
     *
     * @brief   Starts this object.
     */

    void Start()
    {
        gameObject.tag = "Equipment";
        initDynamicMembers();
        
    }

    /**
     * @fn  void OnMouseUp()
     *
     * @brief   Executes the mouse up action.
     */

    void OnMouseUp()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D[] hit = Physics2D.RaycastAll(ray.origin, ray.direction);
        if (hit.Length > 1)
        {
            for (int i = 0; i < hit.Length; i++)
            {
                TrashScript ts = hit[i].collider.gameObject.GetComponent<TrashScript>();
                if (ts != null)
                {
                    ts.DestroyEquipment(gameObject);
                }
            }
        }
    }

    /**
     * @fn  abstract public void initStaticMembers();
     *
     * @brief   Init static members.
     */

    abstract public void initStaticMembers();

    /**
     * @fn  abstract public void initDynamicMembers();
     *
     * @brief   Init dynamic members.
     */

    abstract public void initDynamicMembers();

    /**
     * @fn  abstract public void showConfigureMenu();
     *
     * @brief   Shows the configure menu.
     */

    abstract public void showConfigureMenu();

    /**
     * @fn  public void closeConfigureMenu()
     *
     * @brief   Closes configure menu.
     */

    public void closeConfigureMenu()
    {
        if (uiPanel == null)
            return;
        Destroy(uiPanel);
    }

    /**
     * @fn  public GameObject getConfigPanel()
     *
     * @brief   Gets configuration panel.
     *
     * @return  The configuration panel.
     */

    public GameObject getConfigPanel()
    {
        return uiPanel;
    }

    /**
     * @fn  public string getEquipmentName()
     *
     * @brief   Gets equipment name.
     *
     * @return  The equipment name.
     */

    public string getEquipmentName()
    {
        return equipmentName;
    }

    public string getName()
    {
        return name;
    }

    public void setName(string newName)
    {
        this.name = newName;
    }

    /**
     * @fn  public float getSpriteScale()
     *
     * @brief   Gets sprite scale.
     *
     * @return  The sprite scale.
     */

    public float getSpriteScale()
    {
        return spriteScale;
    }

    /**
     * @fn  public void IsContainer(bool isContainer)
     *
     * @brief   Is container.
     *
     * @param   isContainer True if this object is container.
     */

    public void IsContainer(bool isContainer)
    {
        this.isContainer = isContainer;
    }

    /**
     * @fn  public bool IsContainer()
     *
     * @brief   Query if this object is container.
     *
     * @return  True if container, false if not.
     */

    public bool IsContainer()
    {
        return this.isContainer;
    }

    /**
     * @fn  public void SetContainerMax(float max)
     *
     * @brief   Sets container maximum.
     *
     * @param   max The maximum.
     */

    public void SetContainerMax(float max)
    {
        this.containerMax = max;
        initDynamicMembers();
    }

    /**
     * @fn  public void SetContainerMin(float min)
     *
     * @brief   Sets container minimum.
     *
     * @param   min The minimum.
     */

    public void SetContainerMin(float min)
    {
        this.containerMin = min;
        initDynamicMembers();
    }

    /**
     * @fn  public void SetContainerMeasurement(string measurement)
     *
     * @brief   Sets container measurement.
     *
     * @param   measurement The measurement.
     */

    public void SetContainerMeasurement(string measurement)
    {
        this.containerMeausrement = measurement;
        initDynamicMembers();
    }

    /**
     * @fn  public void SetSpriteScale(float scale)
     *
     * @brief   Sets sprite scale.
     *
     * @param   scale   The scale.
     */

    public void SetSpriteScale(float scale)
    {
        this.spriteScale = scale;
        initDynamicMembers();
    }

    /**
     * @fn  abstract public void addToGameObject(GameObject gameObject);
     *
     * @brief   Adds to the game object.
     *
     * @param   gameObject  The game object.
     */

    abstract public void addToGameObject(GameObject gameObject);

    /**
     * @fn  public void hideEquipment()
     *
     * @brief   Hides the equipment.
     */

    public void hideEquipment()
    {
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        if(sr != null)
        {
            sr.enabled = false;
        }
        BoxCollider2D bc = gameObject.GetComponent<BoxCollider2D>();
        if(bc != null)
        {
            bc.enabled = false;
        }
        foreach(SpriteRenderer childSR in GetComponentsInChildren<SpriteRenderer>())
        {
            childSR.enabled = false;
        }
    }

    /**
     * @fn  public void showEquipment()
     *
     * @brief   Shows the equipment.
     */

    public void showEquipment()
    {
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        if (sr != null)
        {
            sr.enabled = true;
        }
        BoxCollider2D bc = gameObject.GetComponent<BoxCollider2D>();
        if (bc != null)
        {
            bc.enabled = true;
        }
        foreach (SpriteRenderer childSR in GetComponentsInChildren<SpriteRenderer>())
        {
            childSR.enabled = true;
        }
    }

    /**
     * @fn  public bool isVisible()
     *
     * @brief   Query if this object is visible.
     *
     * @return  True if visible, false if not.
     */

    public bool isVisible()
    {
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        if (sr != null)
        {
            return sr.enabled;
        }
        return false;
    }

    /**
     * @fn  public bool isCollider()
     *
     * @brief   Query if this object is collider.
     *
     * @return  True if collider, false if not.
     */

    public bool isCollider()
    {
        BoxCollider2D bc = gameObject.GetComponent<BoxCollider2D>();
        if (bc != null)
        {
            return bc.enabled;
        }

        return false;
    }

}
