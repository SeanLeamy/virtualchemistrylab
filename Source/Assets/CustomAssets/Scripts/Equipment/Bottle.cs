﻿using UnityEngine;
using System.Collections;
using System;


public class Bottle : Equipment
{

    private Sprite fillerSprite;

    public override void showConfigureMenu()
    {

    }

    public override void initStaticMembers()
    {
        panelTitle = "Bottle";
        itemName = "Bottle";
        spritePath = "Sprites/Equipment/bottle";
        equipmentSprite = Resources.Load<Sprite>(spritePath);
        spriteScale = 0.3f;
        string fillerSpritePath = "Sprites/Equipment/bottleFiller";
        fillerSprite = Resources.Load<Sprite>(fillerSpritePath);
    }

    public override void initDynamicMembers()
    {
        equipmentName = string.Format("Bottle {0} {1}", this.containerMax, this.containerMeausrement);
        panelDesc = string.Format("Bottle ({0})", this.containerMeausrement);
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateName()
    {
        this.name = string.Format("{0} {1} {2}", this.itemName, this.containerMax, this.containerMeausrement);
    }


    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<Draggable>();
        equipmentGameObject.AddComponent<DraggableContainer>();
        Container container = equipmentGameObject.AddComponent<Container>();
        container.setMin(this.containerMin);
        container.setMax(this.containerMax);
        container.setMeasurement(this.containerMeausrement);
        container.initFillerSprite(fillerSprite);
        container.initFillerColor(new Color(1, 0, 1, 1));
        Tooltip tooltip = equipmentGameObject.AddComponent<Tooltip>();
        tooltip.addToTooltip(equipmentName);
    }
}

