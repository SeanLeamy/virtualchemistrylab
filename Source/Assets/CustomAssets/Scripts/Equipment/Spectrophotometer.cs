﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class Spectrophotometer : Equipment {

    Sprite openSprite;
    public Cuvette cuvette;
    private bool modifyCollider = false;
    InputField displayField;

    Mode mode = Mode.VIS;                 // Defautlt mode is VIS (UV NYI)
    int visMin = 390;
    int visMax = 700;
    int uvMin = 250;
    int uvMax = 390;

    ReadingType readingType = ReadingType.Absorbance; // Default mode is Absorbance (Transmittance NYI)
    float zeroValue = 0;
    float absorbance;
    float transmittance;
    int wavelength = 390; // Default for both VIS and UV
    bool showNA = false;


    enum Mode {UV, VIS};
    enum ReadingType {Absorbance, Transmittance};

    private static string strEmpty = "Insert a Cuvette";

    // UI
    private static string screenSpritePath = "Sprites/Equipment/spectrophotometerScreen";
    private GameObject seg1, seg2, seg3, seg4;

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {
        panelTitle = "Spectrophotometer";
        panelDesc = string.Format("Spectrophotometer");
        equipmentName = string.Format("Spectrophotometer");
        name = equipmentName;
        spritePath = "Sprites/Equipment/spectrophotometerClosed";
        equipmentSprite = Resources.Load<Sprite>(spritePath);
        string openSpritePath = "Sprites/Equipment/spectrophotometerOpen";
        openSprite = Resources.Load<Sprite>(openSpritePath);
        spriteScale = 0.3f;
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {
        if (!gameObject.name.Equals("LabManager"))
        {
            initUI();
            openLid();
        }
            
    }

    /**
     * @fn  void Update()
     *
     * @brief   Update is called once per frame.
     */

    void Update()
    {
        if (modifyCollider == false)
        {
            // Modify the box collider to fit the image of the spectrophotometer
            BoxCollider2D boxCol = this.GetComponent<BoxCollider2D>();
            if (boxCol != null)
            {
                // Edit the box collider size and offset
                float ySizeDeduction = (boxCol.size.y * 0.22f);
                float ySize = boxCol.size.y - ySizeDeduction;
                Vector2 size = new Vector2(boxCol.size.x, ySize);
                float yOffset = 0 - (ySizeDeduction / 2);
                Vector2 offset = new Vector2(boxCol.offset.x, yOffset);
                boxCol.size = size;
                boxCol.offset = offset;
                modifyCollider = true;
            }
            else
            {
                Debug.Log("Couldn't find BoxCollider2D to edit offset and size");
            }
            
        }
        calculateResults();
    }

    /**
     * @fn  private void calculateResults()
     *
     * @brief   Calculates the results.
     */

    private void calculateResults()
    {
        showNA = true;
        if (!isEmpty())
        {
            Container container = cuvette.gameObject.GetComponent<Container>();
            Dictionary<Material, float> materials = container.getMaterials();
            if(materials.Count == 1)
            {
                Material material = materials.Keys.ElementAt(0);
                if(material is PNitrophenol)
                {
                    PNitrophenol p = material as PNitrophenol;
                    absorbance = zeroValue + p.absorbance;
                    showNA = false;
                }
            }
        }
        updateDisplay();
    }

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {
        throw new NotImplementedException();
    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<Draggable>();
        Tooltip tooltip = equipmentGameObject.AddComponent<Tooltip>();
        tooltip.addToTooltip(equipmentName);
    }

    /**
     * @fn  public void openLid()
     *
     * @brief   Opens the lid.
     */

    public void openLid()
    {
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        if (sr == null)
            return;
        sr.sprite = openSprite;
    }

    /**
     * @fn  public void closeLid()
     *
     * @brief   Closes the lid.
     */

    public void closeLid()
    {
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        if (sr == null)
            return;
        sr.sprite = equipmentSprite;
    }

    /**
     * @fn  public bool isEmpty()
     *
     * @brief   Query if this object is empty.
     *
     * @return  True if empty, false if not.
     */

    public bool isEmpty()
    {
        if (cuvette != null)
            return false;
        return true;
    }

    /**
     * @fn  public void assignCuvette(Cuvette cuvette)
     *
     * @brief   Assign cuvette.
     *
     * @param   cuvette The cuvette.
     */

    public void assignCuvette(Cuvette cuvette)
    {
        cuvette.hideEquipment();
        this.cuvette = cuvette;
        closeLid();
        updateDisplay();
    }

    /**
     * @fn  public void removeCuvette()
     *
     * @brief   Removes the cuvette.
     */

    public void removeCuvette()
    {
        cuvette.showEquipment();
        this.cuvette = null;
        openLid();
        updateDisplay();
    }

    /**
     * @fn  void aButtonClick()
     *
     * @brief   Button click.
     */

    void aButtonClick()
    {
        readingType = ReadingType.Absorbance;
        updateDisplay();
    }

    /**
     * @fn  void tButtonClick()
     *
     * @brief   Button click.
     */

    void tButtonClick()
    {
        readingType = ReadingType.Transmittance;
        updateDisplay();
    }

    /**
     * @fn  void zeroButtonClick()
     *
     * @brief   Zero button click.
     */

    void zeroButtonClick()
    {
        switch (readingType)
        {
            case ReadingType.Absorbance:
                zeroValue = absorbance;
                break;
            case ReadingType.Transmittance:
                zeroValue = transmittance;
                break;
            default:
                zeroValue = 0;
                break;
        }
        updateDisplay();
    }

    /**
     * @fn  void downButtonClick()
     *
     * @brief   Down button click.
     */

    void downButtonClick()
    {
        if (mode == Mode.VIS)
        {
            if (wavelength > visMin)
            {
                wavelength -= 5;
            }
        }
        else
        {
            if (wavelength > uvMin)
            {
                wavelength -= 5;
            }
        }
        updateDisplay();
    }

    /**
     * @fn  void upButtonClick()
     *
     * @brief   Up button click.
     */

    void upButtonClick()
    {
        if(mode == Mode.VIS)
        {
            if(wavelength < visMax)
            {
                wavelength += 5;
            }
        }
        else
        {
            if(wavelength < uvMax)
            {
                wavelength += 5;
            }
        }
        updateDisplay();
    }

    /**
     * @fn  void eButtonClick()
     *
     * @brief   Button click.
     */

    void eButtonClick()
    {
        if(this.cuvette != null)
        {
            removeCuvette();
        }
    }

    /**
     * @fn  void uvButtonClick()
     *
     * @brief   Uv button click.
     */

    void uvButtonClick()
    {
        mode = Mode.UV;
        if(wavelength > uvMax)
            wavelength = uvMax;
        updateDisplay();
    }

    /**
     * @fn  void visButtonClick()
     *
     * @brief   Vis button click.
     */

    void visButtonClick()
    {
        mode = Mode.VIS;
        if (wavelength < visMin)
            wavelength = visMin;
        updateDisplay();
    }

    /**
     * @fn  void updateDisplay()
     *
     * @brief   Updates the display.
     */

    void updateDisplay()
    {
        string seg1Str = "Mode: " + mode.ToString();
        string seg2Str, seg4Str;
        if (isEmpty())
        {
            seg2Str = "Ready";
            seg4Str = "Insert Cuvette";
        }
        else
        {
            if(mode == Mode.UV)
            {
                seg2Str = "N/A"; // UV - NYI
                seg4Str = "N/A";
            }
            else
            {
                seg2Str = readingType.ToString() + ":";
                switch (readingType)
                {
                    case ReadingType.Absorbance:
                        seg4Str = absorbance.ToString();
                        break;
                    case ReadingType.Transmittance:
                        seg4Str = "N/A"; // Not Yet Implemented
                        // seg4Str = transmittance + "%";
                        break;
                    default:
                        seg4Str = "error";
                        break;
                }
                if (showNA)
                    seg4Str = "N/A";
            }
        }
        string seg3Str = "WL(nm): " + wavelength;
        setText(seg1, seg1Str);
        setText(seg2, seg2Str);
        setText(seg3, seg3Str);
        setText(seg4, seg4Str);

    }

    /**
     * @fn  void setText(GameObject gameObject, string text)
     *
     * @brief   Sets a text.
     *
     * @param   gameObject  The game object.
     * @param   text        The text.
     */

    void setText(GameObject gameObject, string text)
    {
        gameObject.GetComponent<Text>().text = text;
    }

    /**
     * @fn  void initUI()
     *
     * @brief   Init user interface.
     */

    void initUI()
    {
        float canvasX = 0f;
        float canvasY = -1.4f;
        float canvasW = 107.31f;
        float canvasH = 67.86f;

        // Setup the Canvas
        GameObject canvasGO = new GameObject();
        canvasGO.name = equipmentName + " Canvas";
        canvasGO.transform.SetParent(transform);
        Canvas canvas = canvasGO.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.WorldSpace;
        CanvasScaler cs = canvasGO.AddComponent<CanvasScaler>();
        cs.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
        cs.dynamicPixelsPerUnit = 1;
        cs.referencePixelsPerUnit = 100;
        GraphicRaycaster gr = canvasGO.AddComponent<GraphicRaycaster>();
        gr.ignoreReversedGraphics = true;
        gr.blockingObjects = GraphicRaycaster.BlockingObjects.None;
        RectTransform canvasRT = canvasGO.GetComponent<RectTransform>();
        canvasRT.localScale = new Vector3(spriteScale, spriteScale, 1);
        canvasRT.sizeDelta = new Vector2(canvasW, canvasH);
        canvasRT.localPosition = new Vector3(canvasX, canvasY, 0);

        // Declare button positions
        float bRowY = -15.27f; //bottom row of buttons Y position
        Vector2 aButtonPos = new Vector2(25.59f, bRowY);
        Vector2 tButtonPos = new Vector2(15, bRowY);
        Vector2 zeroButtonPos = new Vector2(4.2f, bRowY);
        Vector2 downButtonPos = new Vector2(-6.6f, bRowY);
        Vector2 upButtonPos = new Vector2(-16.9f, bRowY);
        Vector2 eButtonPos = new Vector2(-27.8f, bRowY);
        Vector2 uvButtonPos = new Vector2(-37.1f, -5.2f);
        Vector2 visButtonPos = new Vector2(-35.6f, 4.5f);

        // Create buttons
        GameObject aButton = createButton("A", canvasGO, aButtonPos, spriteScale);
        GameObject tButton = createButton("T", canvasGO, tButtonPos, spriteScale);
        GameObject zeroButton = createButton("Zero", canvasGO, zeroButtonPos, spriteScale);
        GameObject downButton = createButton("Down", canvasGO, downButtonPos, spriteScale);
        GameObject upButton = createButton("Up", canvasGO, upButtonPos, spriteScale);
        GameObject eButton = createButton("E", canvasGO, eButtonPos, spriteScale);
        GameObject uvButton = createButton("UV", canvasGO, uvButtonPos, spriteScale);
        GameObject visButton = createButton("VIS", canvasGO, visButtonPos, spriteScale);

        // Add Text to buttons
        addTextToButton(aButton.name, aButton);
        addTextToButton(tButton.name, tButton);
        addTextToButton(zeroButton.name, zeroButton);
        addTextToButton(eButton.name, eButton);
        addTextToButton(uvButton.name, uvButton);
        addTextToButton(visButton.name, visButton);

        // Font has no up or down arrows supported in unicode, to achieve up and down arrows rotate > or < on the Z axis and modify pivots to adjust to center
        /*
        Down Arrow
        pivot    x: 0.75, y: 0.45
        rotation z: 270

        Up Arrow
        pivot    x: 0.35, y: 0.45
        rotation z: 90
         */
        // Up Arrow
        GameObject upTextGO = addTextToButton(">", upButton);
        RectTransform utRT = upTextGO.GetComponent<RectTransform>();
        utRT.Rotate(new Vector3(0, 0, 90f));
        utRT.pivot = new Vector2(0.35f, 0.45f);

        // Down Arrow 
        GameObject downTextGO = addTextToButton(">", downButton);
        RectTransform dtRT = downTextGO.GetComponent<RectTransform>();
        dtRT.Rotate(new Vector3(0, 0, 270f));
        dtRT.pivot = new Vector2(0.75f, 0.45f);
        

        // Attach functions to buttons
        aButton.GetComponent<Button>().onClick.AddListener(() => aButtonClick());
        tButton.GetComponent<Button>().onClick.AddListener(() => tButtonClick());
        zeroButton.GetComponent<Button>().onClick.AddListener(() => zeroButtonClick());
        downButton.GetComponent<Button>().onClick.AddListener(() => downButtonClick());
        upButton.GetComponent<Button>().onClick.AddListener(() => upButtonClick());
        eButton.GetComponent<Button>().onClick.AddListener(() => eButtonClick());
        uvButton.GetComponent<Button>().onClick.AddListener(() => uvButtonClick());
        visButton.GetComponent<Button>().onClick.AddListener(() => visButtonClick());

        // Add Display Screen
        GameObject screenGO = new GameObject();
        screenGO.transform.SetParent(canvasGO.transform);
        screenGO.name = "Screen";
        RectTransform screenRT = screenGO.AddComponent<RectTransform>();
        screenRT.localScale = new Vector3(spriteScale, spriteScale, 1f);
        screenRT.sizeDelta = new Vector2(236.1f, 67.2f);
        screenRT.localPosition = new Vector3(6.8f, 0, 0);
        Image screenImage = screenGO.AddComponent<Image>();
        Sprite screenSprite = Resources.Load<Sprite>(screenSpritePath);
        screenImage.sprite = screenSprite;
        screenImage.type = Image.Type.Sliced;
        // Add Text Segments
        float segmentScale = 1;
        Vector2 segmentSizeDelta = new Vector2(111.9f, 27.8f);
        float segX = 55.95f;
        float segY = 13.0f;
        Vector2 segOnePos = new Vector2(0 - segX, segY);
        Vector2 segTwoPos = new Vector2(segX, segY);
        Vector2 segThreePos = new Vector2(0 - segX, 0 - segY);
        Vector2 segFourPos = new Vector2(segX, 0 - segY);

        seg1 = createTextSegment(
            "Seg1", screenGO,
            segOnePos, segmentSizeDelta,
            segmentScale, TextAnchor.MiddleLeft);

        seg2 = createTextSegment(
            "Seg2", screenGO,
            segTwoPos, segmentSizeDelta,
            segmentScale, TextAnchor.MiddleRight);

        seg3 = createTextSegment(
            "Seg3", screenGO,
            segThreePos, segmentSizeDelta,
            segmentScale, TextAnchor.MiddleLeft);

        seg4 = createTextSegment(
            "Seg4", screenGO,
            segFourPos, segmentSizeDelta,
            segmentScale, TextAnchor.MiddleRight);

        updateDisplay();

    }

    /**
     * @fn  GameObject createButton( string buttonName, GameObject parentGO, Vector2 pos, float scale)
     *
     * @brief   Creates a button.
     *
     * @param   buttonName  Name of the button.
     * @param   parentGO    The parent go.
     * @param   pos         The position.
     * @param   scale       The scale.
     *
     * @return  The new button.
     */

    GameObject createButton(
        string buttonName, GameObject parentGO,
        Vector2 pos, float scale)
    {
        int padLeft, padRight, padTop, padBottom;
        padLeft = padRight = 6;
        padTop = padBottom = 2;
        return createButton(buttonName, parentGO, pos, scale, padLeft, padRight, padTop, padBottom);
    }

    /**
     * @fn  GameObject createButton( string buttonName, GameObject parentGO, Vector2 pos, float scale, int padLeft, int padRight, int padTop, int padBottom)
     *
     * @brief   Creates a button.
     *
     * @param   buttonName  Name of the button.
     * @param   parentGO    The parent go.
     * @param   pos         The position.
     * @param   scale       The scale.
     * @param   padLeft     The pad left.
     * @param   padRight    The pad right.
     * @param   padTop      The pad top.
     * @param   padBottom   The pad bottom.
     *
     * @return  The new button.
     */

    GameObject createButton(
        string buttonName, GameObject parentGO,
        Vector2 pos, float scale,
        int padLeft, int padRight,
        int padTop, int padBottom)
    {
        // Create the button GO
        GameObject buttonGO = new GameObject();
        buttonGO.name = buttonName;
        buttonGO.transform.SetParent(parentGO.transform);
        RectTransform buttonRT = buttonGO.AddComponent<RectTransform>();
        buttonRT.localScale = new Vector3(scale, scale, 1);
        buttonRT.localPosition = pos;
        Image image = buttonGO.AddComponent<Image>();
        image.sprite = uiController.buttonImage;
        image.type = Image.Type.Sliced;
        Button button = buttonGO.AddComponent<Button>();
        button.targetGraphic = image;
        HorizontalLayoutGroup hlg = buttonGO.AddComponent<HorizontalLayoutGroup>();
        hlg.padding = new RectOffset(padLeft, padRight, padTop, padBottom);
        ContentSizeFitter csf = buttonGO.AddComponent<ContentSizeFitter>();
        csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;

        return buttonGO;
    }

    /**
     * @fn  GameObject addTextToButton(string str, GameObject buttonGO)
     *
     * @brief   Adds a text to button to 'buttonGO'.
     *
     * @param   str         The string.
     * @param   buttonGO    The button go.
     *
     * @return  A GameObject.
     */

    GameObject addTextToButton(string str, GameObject buttonGO)
    {
        // Create the text GO
        GameObject textGO = new GameObject();
        textGO.name = buttonGO.name + " Text";
        textGO.transform.SetParent(buttonGO.transform);
        RectTransform textRT = textGO.AddComponent<RectTransform>();
        textRT.localScale = new Vector3(1, 1, 1);
        Text text = textGO.AddComponent<Text>();
        text.text = str;
        text.fontStyle = FontStyle.Normal;
        text.resizeTextForBestFit = true;
        text.font = uiController.textFont;
        text.color = Color.black;
        text.alignment = TextAnchor.UpperLeft;
        ContentSizeFitter csf = textGO.AddComponent<ContentSizeFitter>();
        csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        return textGO;
    }

    /**
     * @fn  GameObject createTextSegment( string name, GameObject parentGO, Vector2 pos, Vector2 sizeDelta, float scale, TextAnchor textAnchor)
     *
     * @brief   Creates text segment.
     *
     * @param   name        The name.
     * @param   parentGO    The parent go.
     * @param   pos         The position.
     * @param   sizeDelta   The size delta.
     * @param   scale       The scale.
     * @param   textAnchor  The text anchor.
     *
     * @return  The new text segment.
     */

    GameObject createTextSegment(
        string name, GameObject parentGO,
        Vector2 pos, Vector2 sizeDelta,
        float scale, TextAnchor textAnchor)
    {
        GameObject segment = new GameObject();
        segment.transform.SetParent(parentGO.transform);
        segment.name = name;
        RectTransform segmentRT = segment.AddComponent<RectTransform>();
        segmentRT.localScale = new Vector3(scale, scale, 1);
        segmentRT.sizeDelta = sizeDelta;
        segmentRT.localPosition = pos;
        Text segmentText = segment.AddComponent<Text>();
        segmentText.font = uiController.textFont;
        segmentText.alignment = textAnchor;
        segmentText.color = Color.black;

        return segment;
    }

    /**
     * @fn  GameObject createInputField( string inputFieldName, GameObject parentGO, Vector2 pos, Vector2 sizeDelta, float scale)
     *
     * @brief   Creates input field.
     *
     * @param   inputFieldName  Name of the input field.
     * @param   parentGO        The parent go.
     * @param   pos             The position.
     * @param   sizeDelta       The size delta.
     * @param   scale           The scale.
     *
     * @return  The new input field.
     */

    GameObject createInputField(
        string inputFieldName, GameObject parentGO,
        Vector2 pos, Vector2 sizeDelta,
        float scale)
    {
        //Create Input Field GO
        GameObject inputFieldGO = new GameObject();
        inputFieldGO.name = inputFieldName;
        inputFieldGO.transform.SetParent(parentGO.transform);
        RectTransform ifRT = inputFieldGO.AddComponent<RectTransform>();
        ifRT.localScale = new Vector3(scale, scale);
        ifRT.sizeDelta = sizeDelta;
        ifRT.anchoredPosition = pos;
        Image ifImg = inputFieldGO.AddComponent<Image>();
        ifImg.sprite = uiController.inputFieldBackground;
        ifImg.type = Image.Type.Sliced;
        ifImg.raycastTarget = true;

        //Create Text GO
        GameObject inputTextGO = new GameObject();
        inputTextGO.name = "Text";
        inputTextGO.transform.SetParent(inputFieldGO.transform);
        RectTransform tRT = inputTextGO.AddComponent<RectTransform>();
        Text text = inputTextGO.AddComponent<Text>();
        text.supportRichText = false; //Input fields do not support rich text
        text.font = uiController.textFont;
        text.fontStyle = FontStyle.Bold;
        text.fontSize = 14;
        text.color = Color.black;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;
        text.alignment = TextAnchor.MiddleLeft;
        tRT.anchorMin = UIController.anchorStretchAllMin;
        tRT.anchorMax = UIController.anchorStretchAllMax;
        tRT.pivot = UIController.anchorStretchAllPivot;
        tRT.sizeDelta = new Vector2(-6, -8);
        tRT.localPosition = Vector2.zero;
        text.resizeTextForBestFit = true;
        text.resizeTextMinSize = 8;

        //Add Input Field Component
        InputField inputField = inputFieldGO.AddComponent<InputField>();
        inputField.transition = Selectable.Transition.ColorTint;
        inputField.targetGraphic = ifImg;
        inputField.characterLimit = 20;
        ColorBlock ifColors = inputField.colors; // Set disabled color to normal color
        ifColors.disabledColor = ifColors.normalColor;
        inputField.colors = ifColors;
        inputField.interactable = false;
        // set text component
        inputField.textComponent = text;
        displayField = inputField;




        return inputFieldGO;
    }

}
