﻿using UnityEngine;
using System.Collections.Generic;
using Assets.CustomAssets.Scripts.Parsers;
using Assets.CustomAssets.Scripts.Manual;

public class LabManager : MonoBehaviour {


    List<Equipment> equipmentList;
    List<Material> materialList;
    List<ManualElement> manualElements;


    void update()
    {
        this.tag = "Untagged";
    }

    /**
     * @fn  void Awake ()
     *
     * @brief   Use this for initialization.
     */

    void Awake () {

        initEquipmentAndMaterials();
        manualElements = ManualParser.getManual("Manuals/Lab1Manual");


    }

    /**
     * @fn  public List<ManualElement> getManaulElements()
     *
     * @brief   Gets manaul elements.
     *
     * @return  The manaul elements.
     */

    public List<ManualElement> getManaulElements()
    {
        return manualElements;
    }

    /**
     * @fn  void initEquipmentAndMaterials()
     *
     * @brief   Init equipment and materials.
     */

    void initEquipmentAndMaterials()
    {
        equipmentList = new List<Equipment>();
        Cuvette cuvette = gameObject.AddComponent<Cuvette>();
        cuvette.SetSpriteScale(cuvette.spriteScale / 1.25f);
        cuvette.SetContainerMax(1f);
        cuvette.SetContainerMeasurement("mL");
        equipmentList.Add(cuvette);

        /* Cuvette cuvetteA = gameObject.AddComponent<Cuvette>();
        cuvetteA.SetContainerMax(3.5f);
        cuvetteA.SetContainerMeasurement("mL");
        equipmentList.Add(cuvetteA); */

        Bottle bottle = gameObject.AddComponent<Bottle>();
        bottle.SetContainerMax(100f);
        bottle.SetContainerMeasurement("mL");
        equipmentList.Add(bottle);

        Spectrophotometer spectrophotometer = gameObject.AddComponent<Spectrophotometer>();
        spectrophotometer.enabled = false;
        equipmentList.Add(spectrophotometer);

        
        materialList = new List<Material>();

        float[] pNitrophenolConcentrations = { 1, 0.65f, 0.25f, 0.125f, 0.0625f, 0.03125f };

        foreach (float f in pNitrophenolConcentrations)
        {
            PNitrophenol p = gameObject.AddComponent<PNitrophenol>();
            p.productConcentration = f;
            materialList.Add(p);
        }


        /* Commented Materials Cut from experiment due to time constraints
        Water water = gameObject.AddComponent<Water>();
        materialList.Add(water);

        AlkalinePhosphatase ap1 = gameObject.AddComponent<AlkalinePhosphatase>();
        ap1.enzymeConcentration = 0.044f;
        ap1.phosphateConcentration = 50;
        ap1.MgClConcentration = 1;
        ap1.pH = 7.0f;
        materialList.Add(ap1);

        AlkalinePhosphatase ap2 = gameObject.AddComponent<AlkalinePhosphatase>();
        ap2.enzymeConcentration = 1.75f;
        ap2.phosphateConcentration = 50;
        ap2.MgClConcentration = 1;
        ap2.pH = 7.0f;
        materialList.Add(ap2);

        PNitrophenylPhosphate pnp1 = gameObject.AddComponent<PNitrophenylPhosphate>();
        pnp1.substrateConcentration = 2400;
        materialList.Add(pnp1);

        PNitrophenylPhosphate pnp2 = gameObject.AddComponent<PNitrophenylPhosphate>();
        pnp2.substrateConcentration = 150;
        materialList.Add(pnp2);

        Aldolase a1 = gameObject.AddComponent<Aldolase>();
        a1.enzymeConcentration = 0.4f;
        a1.phosphateConcentration = 50;
        a1.MgCl = 1;
        a1.pH = 7.0f;
        materialList.Add(a1);

        BovineSerumAlbumin b1 = gameObject.AddComponent<BovineSerumAlbumin>();
        b1.mgmL = 0.4f;
        b1.phosphateBuffer = 50;
        b1.MgCl = 1;
        b1.pH = 7.0f;
        materialList.Add(b1);
        */
    }

    /**
     * @fn  void Start()
     *
     * @brief   Starts this object.
     */

    void Start()
    {
        
    }

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

    void Update () {
        this.tag = "Untagged";
    }

    /**
     * @fn  public List<Equipment> getEquipmentList()
     *
     * @brief   Gets equipment list.
     *
     * @return  The equipment list.
     */

    public List<Equipment> getEquipmentList()
    {
        return equipmentList;
    }

    /**
     * @fn  public List<Material> getMaterialList()
     *
     * @brief   Gets material list.
     *
     * @return  The material list.
     */

    public List<Material> getMaterialList()
    {
        return materialList;
    }

    /**
     * @fn  public static GameObject getLabManagerGO()
     *
     * @brief   Gets lab manager go.
     *
     * @return  The lab manager go.
     */

    public static GameObject getLabManagerGO()
    {
        return GameObject.Find("LabManager");
    }

    public void renameItem()
    {
        Equipment target = this.gameObject.GetComponent<RightClick>().target;
        target.equipmentName = "dsafdsfadsfs";
    }

    public void deleteItem()
    {
        GameObject trashSprite;
        Equipment target = this.gameObject.GetComponent<RightClick>().target;
        trashSprite = GameObject.Find("TrashSprite");
        TrashScript ts = trashSprite.GetComponent<TrashScript>();

        ts.DestroyEquipment(target.gameObject);
    }

    public void deleteAllExistingItems()
    {
        GameObject[] existingEquipment = GameObject.FindGameObjectsWithTag("Equipment");

        foreach (GameObject equipment in existingEquipment)
        {
            Debug.Log("Destroying " + equipment.name);
            Destroy(equipment);
        }
    }
}
