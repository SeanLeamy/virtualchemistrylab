﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class EquipmentPanelContent : MonoBehaviour {

    private LabManager labManager;
    List<Equipment> equipmentList;
    UIController uiController;

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

    void Start () {

        labManager = GameObject.Find("LabManager").GetComponent<LabManager>();
        if (uiController == null)
        {
            uiController = GameObject.Find("Utils").GetComponent<UIController>();
        }
        UpdateEquipmentList();
    }

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

	void Update () {

    }

    /**
     * @fn  public void UpdateEquipmentList()
     *
     * @brief   Updates the equipment list.
     */

    public void UpdateEquipmentList()
    {
        equipmentList = labManager.getEquipmentList();
        ConfigurePanel();
    }

    /**
     * @fn  public void ConfigurePanel()
     *
     * @brief   Configure panel.
     */

    public void ConfigurePanel()
    {
        ConfigurePanel("");
    }

    /**
     * @fn  public void ConfigurePanel(string filter)
     *
     * @brief   Loops through the equipment list and 
     *          adds each piece of equipment to the
     *          equipment panel.
     *
     * @param   filter  Specifies the filter.
     */

    public void ConfigurePanel(string filter)
    {
        DestroyChildren();
        foreach (Equipment equipment in equipmentList)
        {
            string name = equipment.getEquipmentName().ToLower();
            filter = filter.ToLower();
            bool canAdd = true;
            if (!string.IsNullOrEmpty(filter))
                if (!name.Contains(filter))
                    canAdd = false;
            if(canAdd)
                AddEquipmentToPanel(equipment);
        }
            
    }

    /**
     * @fn  private void DestroyChildren()
     *
     * @brief   Destroys the children.
     */

    private void DestroyChildren()
    {
        foreach (Transform child in gameObject.transform)
            Destroy(child.gameObject);
    }

    /**
     * @fn  private void AddEquipmentToPanel(Equipment equipment)
     *
     * @brief   Creates an object spawner for an equipment item
     *          and adds it the to equipment panel.
     *
     * @param   equipment   The equipment.
     */

    private void AddEquipmentToPanel(Equipment equipment)
    {
        float buttonW = 80f;
        float buttonH = 80f;
        Vector2 buttonPos = Vector2.zero;
        float textW = 80f;
        float textH = 35;
        Vector2 textPos = new Vector2(0, -textH);
        GameObject equipmentSpawnerGO = new GameObject();
        equipmentSpawnerGO.transform.SetParent(gameObject.transform);
        equipmentSpawnerGO.name = equipment.getEquipmentName() + " Spawner";
        equipmentSpawnerGO.AddComponent<LayoutElement>();
        GameObject buttonGO = new GameObject();
        buttonGO.transform.SetParent(equipmentSpawnerGO.transform);
        RectTransform bRT = buttonGO.AddComponent<RectTransform>();
        bRT.anchorMin = UIController.anchorTopCenterMin;
        bRT.anchorMax = UIController.anchorTopCenterMax;
        bRT.pivot = UIController.anchorTopCenterPivot;
        bRT.sizeDelta = new Vector2(buttonW, buttonH);
        bRT.anchoredPosition = buttonPos;
        Image bImg = buttonGO.AddComponent<Image>();
        bImg.sprite = equipment.getEquipmentSprite();
        bImg.preserveAspect = true;
        Button bBtn = buttonGO.AddComponent<Button>();
        
        // Object spawning code
        bBtn.onClick.AddListener(() => 
        {
            GameObject equipmentGameObject = new GameObject();
            Utils.CopyComponent(equipment, equipmentGameObject);
            equipmentGameObject.name = equipment.getEquipmentName();
            Transform goT = equipmentGameObject.GetComponent<Transform>();
            goT.position = Vector3.zero;
            SpriteRenderer goSR = equipmentGameObject.AddComponent<SpriteRenderer>();
            goSR.sprite = equipment.getEquipmentSprite();
            equipment.addToGameObject(equipmentGameObject);
            float scale = equipment.getSpriteScale();
            goT.localScale = new Vector3(scale, scale, 1);
        });
        GameObject textGO = new GameObject();
        textGO.transform.SetParent(buttonGO.transform);
        RectTransform tRT = textGO.AddComponent<RectTransform>();
        tRT.anchorMin = UIController.anchorBottomCenterMin;
        tRT.anchorMax = UIController.anchorBottomCenterMax;
        tRT.pivot = UIController.anchorBottomCenterPivot;
        tRT.sizeDelta = new Vector2(textW, textH);
        tRT.anchoredPosition = textPos;
        Text tTxt = textGO.AddComponent<Text>();
        tTxt.supportRichText = false;
        tTxt.alignment = TextAnchor.UpperCenter;
        tTxt.text = equipment.getEquipmentName();
        tTxt.resizeTextForBestFit = true;
        tTxt.resizeTextMinSize = 8;
        tTxt.resizeTextMaxSize = 12;
        tTxt.font = uiController.textFont;
        tTxt.color = Color.black;
        
    }


}
