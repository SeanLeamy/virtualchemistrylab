﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour {

    private Vector3 screenPoint;
    private Vector3 offset;
    private GameObject clone;
    bool hitUI;
    public bool isDraggable;

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

    void Start () {

        hitUI = false;
        isDraggable = false;
        if(gameObject.GetComponent<Collider>() == null)
        {
            gameObject.AddComponent<BoxCollider2D>();
        }
        isDraggable = true;
            
	}

    /**
     * @fn  void OnMouseDown()
     *
     * @brief   Executes the mouse down action.
     */

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject() || hitUI)
        {
            hitUI = true;
            return; // Prevent clicking through UI
        }
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        if (sr != null)
        {
            sr.enabled = false; // disabling and re-enabling the sprite renderer forces the sprite to be drawn on top of sprites on the same layer and Order in Layer
            sr.enabled = true;
            sr.sortingOrder = 1;
        }
        BoxCollider2D bc = gameObject.GetComponent<BoxCollider2D>();
        if(bc != null)
        {
            bc.enabled = false;
            bc.enabled = true;
        }
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
        
    }

    /**
     * @fn  void OnMouseDrag()
     *
     * @brief   Executes the mouse drag action.
     */

    void OnMouseDrag()
    {
        if (EventSystem.current.IsPointerOverGameObject() || hitUI)
        {
            hitUI = true;
            return; // Prevent clicking through UI
        }
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        cursorPosition = new Vector3(cursorPosition.x, cursorPosition.y, 0f);
        EquipmentMenu eqpMenu = gameObject.GetComponent<EquipmentMenu>();
        if (eqpMenu == null)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
                transform.position = cursorPosition;
        }
        else if (eqpMenu.menuGameObject == null)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
                transform.position = cursorPosition;
        }
    }

    /**
     * @fn  void OnMouseUp()
     *
     * @brief   Executes the mouse up action.
     */

    void OnMouseUp()
    {
        hitUI = false;
        SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
        if(sr != null)
        {
            sr.sortingOrder = 0;
        }
    }

}
