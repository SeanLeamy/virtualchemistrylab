﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class MaterialPanelContent : MonoBehaviour {

    private LabManager labManager;
    List<Material> materialList;
    UIController uiController;

    /**
     * @fn  void Start()
     *
     * @brief   Use this for initialization.
     */

    void Start()
    {
        labManager = GameObject.Find("LabManager").GetComponent<LabManager>();
        if (uiController == null)
        {
            uiController = GameObject.Find("Utils").GetComponent<UIController>();
        }
        materialList = labManager.getMaterialList();
        ConfigurePanel();
    }

    /**
     * @fn  void Update()
     *
     * @brief   Update is called once per frame.
     */

    void Update()
    {

    }

    /**
     * @fn  public void ConfigurePanel()
     *
     * @brief   Configure panel.
     */

    public void ConfigurePanel()
    {
        ConfigurePanel("");
    }

    /**
     * @fn  public void ConfigurePanel(string filter)
     *
     * @brief   Configure panel.
     *
     * @param   filter  Specifies the filter.
     */

    public void ConfigurePanel(string filter)
    {
        DestroyChildren();
        foreach (Material material in materialList)
        {
            string name = material.getMaterialName().ToLower();
            filter = filter.ToLower();
            bool canAdd = true;
            if (!string.IsNullOrEmpty(filter))
                if (!name.Contains(filter))
                    canAdd = false;
            if (canAdd)
                AddMaterialToPanel(material);
        }

    }

    /**
     * @fn  private void DestroyChildren()
     *
     * @brief   Destroys the children.
     */

    private void DestroyChildren()
    {
        foreach (Transform child in gameObject.transform)
            Destroy(child.gameObject);
    }

    /**
     * @fn  private void AddMaterialToPanel(Material material)
     *
     * @brief   Adds a material to panel.
     *
     * @param   material    The material.
     */

    private void AddMaterialToPanel(Material material)
    {
        float buttonW = 80f;
        float buttonH = 80f;
        Vector2 buttonPos = Vector2.zero;
        float textW = 80f;
        float textH = 35;
        Vector2 textPos = new Vector2(0, -textH);
        GameObject materialSelectorGO = new GameObject();
        materialSelectorGO.transform.SetParent(gameObject.transform);
        materialSelectorGO.name = material.getMaterialName() + " Spawner";
        materialSelectorGO.AddComponent<LayoutElement>();
        GameObject buttonGO = new GameObject();
        buttonGO.transform.SetParent(materialSelectorGO.transform);
        RectTransform bRT = buttonGO.AddComponent<RectTransform>();
        bRT.anchorMin = UIController.anchorTopCenterMin;
        bRT.anchorMax = UIController.anchorTopCenterMax;
        bRT.pivot = UIController.anchorTopCenterPivot;
        bRT.sizeDelta = new Vector2(buttonW, buttonH);
        bRT.anchoredPosition = buttonPos;
        Image bImg = buttonGO.AddComponent<Image>();
        bImg.sprite = material.getMaterialSprite();
        bImg.preserveAspect = true;
       // Button bBtn = buttonGO.AddComponent<Button>();
       // bBtn.onClick.AddListener(() =>
       // {
           // GameObject materialGameObject = new GameObject();
            //materialGameObject.name = material.getMaterialName();
           // RectTransform 

            /*Transform goT = materialGameObject.GetComponent<Transform>();
            goT.position = Vector3.zero;
            SpriteRenderer goSR = materialGameObject.AddComponent<SpriteRenderer>();
            goSR.sprite = material.getMaterialSprite();
            material.addToGameObject(materialGameObject);
            float scale = materialGameObject.GetComponent<Material>().getSpriteScale();
            goT.localScale = new Vector3(scale, scale, 1);*/
        //});
        GameObject textGO = new GameObject();
        textGO.transform.SetParent(buttonGO.transform);
        RectTransform tRT = textGO.AddComponent<RectTransform>();
        tRT.anchorMin = UIController.anchorBottomCenterMin;
        tRT.anchorMax = UIController.anchorBottomCenterMax;
        tRT.pivot = UIController.anchorBottomCenterPivot;
        tRT.sizeDelta = new Vector2(textW, textH);
        tRT.anchoredPosition = textPos;
        Text tTxt = textGO.AddComponent<Text>();
        tTxt.supportRichText = false;
        tTxt.alignment = TextAnchor.UpperCenter;
        tTxt.text = material.getMaterialName();
        tTxt.resizeTextForBestFit = true;
        tTxt.resizeTextMinSize = 8;
        tTxt.resizeTextMaxSize = 12;
        tTxt.font = uiController.textFont;
        tTxt.color = Color.black;


        buttonGO.AddComponent<DraggableMaterial>();
        Utils.CopyComponent(material, buttonGO);
        // buttonGO.GetComponent<Material>().enabled = false;

    }
}
