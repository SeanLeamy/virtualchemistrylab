﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DraggableContainer : MonoBehaviour {


    private Texture2D cursorGreenTick;
    private Texture2D cursorRedCross;
    private GameObject containerGO;
    private GameObject spectrophotometerGO;
    private GameObject trashGO;
    private Vector3 startPos;

    bool resetPosOnMouseUp;
    bool isExistingPopup;

    /**
     * @fn  void Awake()
     *
     * @brief   Awakes this object.
     */
    void Awake()
    {
        string cursorPath = "Sprites/GUI/Cursors/";
        cursorGreenTick = Resources.Load<Texture2D>(cursorPath + "greenTick");
        cursorRedCross = Resources.Load<Texture2D>(cursorPath + "redCross");
    }

    /**
     * @fn  void OnMouseDown()
     *
     * @brief   Executes the mouse down action.
     */

    void OnMouseDown()
    {
        startPos = gameObject.transform.position;
        isExistingPopup = IsExistingPopup();
    }

    /**
     * @fn  void OnMouseDrag()
     *
     * @brief   Executes the mouse drag action.
     */

    void OnMouseDrag()
    {
        trashGO = null;
        if (isExistingPopup) return;
        if (EventSystem.current.IsPointerOverGameObject())
            return; // Prevent clicking through UI

        resetPosOnMouseUp = false;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D[] hit = Physics2D.RaycastAll(ray.origin, ray.direction);
        if (hit.Length > 1)
        {
            Cuvette cuvette = this.gameObject.GetComponent<Cuvette>();
            for(int i=0; i<hit.Length; i++)
            {
                Spectrophotometer spectrophotometer = hit[i].collider.GetComponent<Spectrophotometer>();
                if (hit[i].transform.gameObject != gameObject.transform.gameObject)
                {
                    // Let other scripts take over if on a trash script
                    TrashScript ts = hit[i].collider.GetComponent<TrashScript>();
                    if (ts != null)
                    {
                        trashGO = hit[i].transform.gameObject;
                        break;
                    }                        
                    Container container = hit[i].collider.GetComponent<Container>();
                    if (container != null)
                    {
                        Cursor.SetCursor(cursorGreenTick, Vector2.zero, CursorMode.Auto);
                        containerGO = hit[i].transform.gameObject;
                        break; // Only check first object hit that's not the object being dragged
                    }
                    else if(cuvette != null && spectrophotometer != null && spectrophotometer.isEmpty())
                    {
                        Cursor.SetCursor(cursorGreenTick, Vector2.zero, CursorMode.Auto);
                        // spectrophotometer.openLid();
                        spectrophotometerGO = hit[i].transform.gameObject;
                        break;
                    }
                    else
                    {
                        Cursor.SetCursor(cursorRedCross, Vector2.zero, CursorMode.Auto);
                        resetPosOnMouseUp = true;
                        containerGO = null;
                        if (spectrophotometerGO != null)
                        {
                            // spectrophotometerGO.GetComponent<Spectrophotometer>().closeLid();
                            spectrophotometerGO = null;
                        }
                        break;
                    }
                }
            }
            

        }
        else
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            containerGO = null;
            if (spectrophotometerGO != null)
            {
                spectrophotometerGO.GetComponent<Spectrophotometer>().closeLid();
                spectrophotometerGO = null;
            }
        }
    }

    /**
     * @fn  void OnMouseUp()
     *
     * @brief   Executes the mouse up action.
     */

    void OnMouseUp()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        if (trashGO != null)
            return;
            
        if (resetPosOnMouseUp)
            resetPos();

        if (isExistingPopup)
            return;

        if (EventSystem.current.IsPointerOverGameObject())
            return; // Prevent clicking through UI

        Container curContainer = gameObject.GetComponent<Container>();
        if (containerGO != null)
        {
            Container selContainer = containerGO.GetComponent<Container>();
            if (selContainer != null)
            {
                selContainer.showPour(curContainer);
                curContainer = null;
            }
        } else if(spectrophotometerGO != null)
        {
            Cuvette cuvette = gameObject.GetComponent<Cuvette>();
            if(cuvette != null)
            {
                spectrophotometerGO.GetComponent<Spectrophotometer>().assignCuvette(cuvette);
                cuvette = null;
            }
        }
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        containerGO = null;
        spectrophotometerGO = null;
    }

    /**
     * @fn  bool IsExistingPopup()
     *
     * @brief   Query if this object is existing popup.
     *
     * @return  True if existing popup, false if not.
     */

    bool IsExistingPopup()
    {
        foreach (GameObject go in GameObject.FindObjectsOfType<GameObject>())
        {
            if (go.tag.Equals("InputPopup"))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @fn  void resetPos()
     *
     * @brief   Resets the position.
     */

    void resetPos()
    {
       gameObject.transform.position = startPos;
        resetPosOnMouseUp = false;
    }


}
