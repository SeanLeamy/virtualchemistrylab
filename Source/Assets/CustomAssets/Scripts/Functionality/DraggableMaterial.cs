﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DraggableMaterial : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    private Vector2 pointerOffset;
    private RectTransform canvasRectTransform;
    private GameObject tempMaterial;
    private RectTransform materialRectTransform;
    private Texture2D cursorGreenTick;
    private Texture2D cursorRedCross;

    /**
     * @fn  void Awake()
     *
     * @brief   Awakes this object.
     */

    void Awake()
    {
        Canvas canvas = GetComponentInParent<Canvas>();
        if (canvas != null)
        {
            canvasRectTransform = canvas.transform as RectTransform;
        }

        string cursorPath = "Sprites/GUI/Cursors/";
        cursorGreenTick = Resources.Load<Texture2D>(cursorPath+"greenTick");
        cursorRedCross = Resources.Load<Texture2D>(cursorPath + "redCross");
    }

    /**
     * @fn  public void OnPointerDown(PointerEventData data)
     *
     * @summary <para></para>
     *
     * @param   data    Current event data.
     */

    public void OnPointerDown(PointerEventData data)
    {
        
        tempMaterial = new GameObject();
        tempMaterial.transform.SetParent(UIController.getCanvas().transform);
        materialRectTransform = tempMaterial.AddComponent<RectTransform>();
        materialRectTransform.sizeDelta = new Vector2(80f, 80f);
        materialRectTransform.position = Input.mousePosition;
        Image img = tempMaterial.AddComponent<Image>();
        img.sprite = gameObject.GetComponent<Image>().sprite;
        img.preserveAspect = true;
        materialRectTransform.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(materialRectTransform, data.position, data.pressEventCamera, out pointerOffset);
    }

    /**
     * @fn  public void OnPointerUp(PointerEventData data)
     *
     * @summary <para></para>
     *
     * @param   data    Current event data.
     */

    public void OnPointerUp(PointerEventData data)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            Container container = hit.collider.GetComponent<Container>();
            if (container != null)
                container.showPour(gameObject.GetComponentInChildren<Material>());
        }
        Destroy(tempMaterial);
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    /**
     * @fn  public void OnDrag(PointerEventData data)
     *
     * @summary <para>
     *          When draging is occuring this will be called every time the cursor is moved.</para>
     *
     * @param   data    Current event data.
     */

    public void OnDrag(PointerEventData data)
    {
        if (materialRectTransform == null)
            return;

        Vector2 pointerPostion = ClampToWindow(data);

        Vector2 localPointerPosition;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            canvasRectTransform, pointerPostion, data.pressEventCamera, out localPointerPosition
        ))
        {
            materialRectTransform.localPosition = localPointerPosition - pointerOffset;
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            Container container = hit.collider.GetComponent<Container>();
            if (container != null)
            {
                Cursor.SetCursor(cursorGreenTick, Vector2.zero, CursorMode.Auto);
            }
            else
            {
                Cursor.SetCursor(cursorRedCross, Vector2.zero, CursorMode.Auto);
            }
                
        }
        else
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

    }

    /**
     * @fn  Vector2 ClampToWindow(PointerEventData data)
     *
     * @brief   Clamp to window.
     *
     * @param   data    The data.
     *
     * @return  A Vector2.
     */

    Vector2 ClampToWindow(PointerEventData data)
    {
        Vector2 rawPointerPosition = data.position;

        Vector3[] canvasCorners = new Vector3[4];
        canvasRectTransform.GetWorldCorners(canvasCorners);

        float clampedX = Mathf.Clamp(rawPointerPosition.x, canvasCorners[0].x, canvasCorners[2].x);
        float clampedY = Mathf.Clamp(rawPointerPosition.y, canvasCorners[0].y, canvasCorners[2].y);

        Vector2 newPointerPosition = new Vector2(clampedX, clampedY);
        return newPointerPosition;
    }


}
