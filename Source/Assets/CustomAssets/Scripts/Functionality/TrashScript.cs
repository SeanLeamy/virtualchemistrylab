﻿using UnityEngine;
using System.Collections;

public class TrashScript : MonoBehaviour {

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

	void Start () {
	
	}

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

	void Update () {
	
	}

    /**
     * @fn  public void DestroyEquipment(GameObject GO)
     *
     * @brief   Destroys the equipment described by GO.
     *
     * @param   GO  The go.
     */

    public void DestroyEquipment(GameObject GO)
    {
        Tooltip tooltip = GO.GetComponent<Tooltip>();
        Destroy(GO);
        tooltip.clearTooltip();
    }

}
