﻿using UnityEngine;
using System.Collections;
using System.Reflection;

public class Utils : MonoBehaviour {

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

	void Start () {
	
	}

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

	void Update () {
	
	}

    /**
     * @fn  static public GameObject getChildGameObject(GameObject go, string byName)
     *
     * @brief   Gets a child game object.
     *
     * @param   go      The go.
     * @param   byName  Name of the by.
     *
     * @return  The child game object.
     */

    static public GameObject getChildGameObject(GameObject go, string byName)
    {

        Transform[] ts = go.transform.GetComponentsInChildren<Transform>();
        foreach (Transform t in ts)
            if (t.gameObject.name.Equals(byName))
                return t.gameObject;
        return null;

    }

    /**
     * @fn  static public Component CopyComponent(Component original, GameObject destination)
     *
     * @brief   Copies the component.
     *
     * @param   original    The original.
     * @param   destination Destination for the.
     *
     * @return  A Component.
     */

    static public Component CopyComponent(Component original, GameObject destination)
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        // Copied fields can be restricted with BindingFlags
        FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
        foreach (FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy;
    }

    
}
