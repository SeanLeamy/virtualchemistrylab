﻿using UnityEngine;
using System.Collections.Generic;

public class Container : MonoBehaviour {

    public Sprite fillerSprite;
    public GameObject fillerObject;
    public Color fillerColor;

    public string name;
    public string measurement;
    public float max;
    public float min;
    public float current = 0;
    public Dictionary<Material, float> materials;

    private Material pourMaterial;
    private Container pourContainer;

    GameObject pourPanel;

    /**
     * @fn  public void setMeasurement(string measurement)
     *
     * @brief   Sets a measurement.
     *
     * @param   measurement The measurement.
     */

    public void setMeasurement(string measurement)
    {
        this.measurement = measurement;
    }

    /**
     * @fn  public string getMeasurement()
     *
     * @brief   Gets the measurement.
     *
     * @return  The measurement.
     */

    public string getMeasurement()
    {
        return measurement;
    }

    /**
     * @fn  public void setMax(float max)
     *
     * @brief   Sets a maximum.
     *
     * @param   max The maximum.
     */

    public void setMax(float max)
    {
        this.max = max;
    }

    /**
     * @fn  public float getMax()
     *
     * @brief   Gets the maximum.
     *
     * @return  The calculated maximum.
     */

    public float getMax()
    {
        return max;
    }

    /**
     * @fn  public void setMin(float min)
     *
     * @brief   Sets a minimum.
     *
     * @param   min The minimum.
     */

    public void setMin(float min)
    {
        this.min = min;
    }

    /**
     * @fn  public float getMin()
     *
     * @brief   Gets the minimum.
     *
     * @return  The calculated minimum.
     */

    public float getMin()
    {
        return max;
    }

    /**
     * @fn  public float getCurrent()
     *
     * @brief   Gets the current.
     *
     * @return  The current.
     */

    public float getCurrent()
    {
        return current;
    }

    /**
     * @fn  public bool deduct(Material mat, float pourAmount)
     *
     * @brief   Deducts.
     *
     * @param   mat         The matrix.
     * @param   pourAmount  The pour amount.
     *
     * @return  True if it succeeds, false if it fails.
     */

    public bool deduct(Material mat, float pourAmount)
    {

        if (pourAmount > current)
            return false;

        this.materials[mat] = this.materials[mat] - pourAmount;
        this.current = this.current - pourAmount;

        return true;
    }

    /**
     * @fn  public Dictionary<Material, float> getMaterials()
     *
     * @brief   Gets the materials.
     *
     * @return  The materials.
     */

    public Dictionary<Material, float> getMaterials()
    {
        return materials;
    }

    public void setMaterials(Dictionary<Material, float> materials)
    {
        this.materials = materials;
    }

    /**
     * @fn  public bool pour(float amount)
     *
     * @brief   add materials.
     *
     * @param   amount  The amount.
     *
     * @return  True if it succeeds, false if it fails.
     */

    public bool pour(float amount)
    {
        if (amount+current > max)
            return false;

        if (pourMaterial != null)
        {
            current += amount;
            if (!materials.ContainsKey(pourMaterial))
            {
                materials.Add(pourMaterial, amount);
            }
            else
            {
                materials[pourMaterial] += amount;
            }
            pourMaterial = null;
        }
        else if (pourContainer != null)
        {
            Dictionary<Material, float> containerMaterials = new Dictionary<Material, float>(pourContainer.getMaterials());
            
            foreach (Material mat in containerMaterials.Keys)
            {
                float matAmount = (amount / pourContainer.getCurrent()) * containerMaterials[mat];
                current += matAmount;
                if (!materials.ContainsKey(mat))
                {
                    materials.Add(mat, matAmount);
                    
                }
                else
                {
                    materials[mat] += matAmount;
                }
                pourContainer.deduct(mat, matAmount);

            }
            pourContainer = null;
        }

        updateColor();
        
        return true;
    }

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

    void Start () {
        materials = new Dictionary<Material, float>();
        fillerObject = new GameObject();
        fillerObject.transform.SetParent(gameObject.transform);
        fillerObject.name = "Filler";
        fillerColor = new Color(1, 1, 1, 0);
        SpriteRenderer foSR = fillerObject.AddComponent<SpriteRenderer>();
        foSR.sprite = this.fillerSprite;
        foSR.sortingOrder = 1;
        fillerObject.GetComponent<SpriteRenderer>().color = fillerColor;
        fillerObject.transform.localScale = new Vector3(1, 1, 1);
    }

    /**
     * @fn  public void initFillerSprite(Sprite fillerSprite)
     *
     * @brief   Init filler sprite.
     *
     * @param   fillerSprite    The filler sprite.
     */

    public void initFillerSprite(Sprite fillerSprite)
    {
        this.fillerSprite = fillerSprite;
    }

    /**
     * @fn  public void initFillerColor(Color color)
     *
     * @brief   Init filler color.
     *
     * @param   color   The color.
     */

    public void initFillerColor(Color color)
    {
        fillerColor = color;
    }

    /**
     * @fn  public void changeFillerColor(Color color)
     *
     * @brief   Change filler color.
     *
     * @param   color   The color.
     */

    public void changeFillerColor(Color color)
    {
        fillerColor = color;
        fillerObject.GetComponent<SpriteRenderer>().color = fillerColor;
    }

    /**
     * @fn  public void showPour(Material material)
     *
     * @brief   Shows the pour.
     *
     * @param   material    The material.
     */

    public void showPour(Material material)
    {
        pourMaterial = material;
        UIController uiController = UIController.getUIController();
        string valueDesc = "Enter amount in (" + measurement + ")";
        pourPanel = uiController.createSimpleNumberCounterPanel("Pour " + material.materialName, valueDesc, min, min, max - current, pour, false); 
    }

    /**
     * @fn  public void showPour(Container container)
     *
     * @brief   Shows the pour.
     *
     * @param   container   The container.
     */

    public void showPour(Container container)
    {
        pourContainer = container;
        UIController uiController = UIController.getUIController();
        string valueDesc = "Enter amount in (" + measurement + ")";
        float maxPour = max-current;
        if (container.getCurrent() < maxPour)
            maxPour = container.getCurrent();
        pourPanel = uiController.createSimpleNumberCounterPanel("Pour " + container.GetComponentInParent<Equipment>().getEquipmentName(), valueDesc, min, min, maxPour, pour, false);
    }

    /**
     * @fn  public void updateColor()
     *
     * @brief   Updates the color.
     */

    public void updateColor()
    {

        foreach (Material mat in materials.Keys)
        {
            if(mat.GetType() == typeof(AlkalinePhosphatase))
            {
                Debug.Log("Found AlkalinePhoshatase");
            }
            changeFillerColor(mat.liquidColor);
        }

    }
	


    

}
