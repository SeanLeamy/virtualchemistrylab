﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RightClick : MonoBehaviour {

    public GameObject rightClickPanel;
    public GameObject trashRightClickPanel;
    public GameObject canvas;
    public Equipment target;
    
    private RectTransform canvasRectTransform;

	
	void Start () {

        canvasRectTransform = canvas.transform as RectTransform;
    }

    
    void Update()
    {
        // Handle mouse clicks

        // Left mouse click
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) {
                
            } else
            {
                DestroyExistingMenus();
            }
        }

        // Right mouse click
        if (Input.GetMouseButtonUp(1))
        {
            DestroyExistingMenus();

            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject.name == "TrashSprite")
                {
                    var mousepos = Input.mousePosition;
                    mousepos.y -= canvasRectTransform.rect.height; //The mouse coordinates are offset by the height of the canvas
                    var rightClickMenu = Instantiate(trashRightClickPanel, mousepos, transform.rotation);
                    rightClickMenu.transform.SetParent(canvas.transform, false);

                } else
                {
                    target = hit.collider.gameObject.GetComponent<Equipment>();

                    var mousepos = Input.mousePosition;

                    mousepos.y -= canvasRectTransform.rect.height; //The mouse coordinates are offset by the height of the canvas
                    var rightClickMenu = Instantiate(rightClickPanel, mousepos, transform.rotation);
                    rightClickMenu.transform.SetParent(canvas.transform, false);
                }
                
            }
        }
    }

    public void DestroyExistingMenus()
    {
        GameObject gameObject;

        gameObject = GameObject.FindWithTag("RightClickMenu");
        Destroy(gameObject);
    }
}
