﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.CustomAssets.Scripts.Manual
{
    public abstract class ManualElement
    {
        // Tags
        public enum Tag {None, h1, p, img, ul, li}
        // Attributes
        public enum TextStyle {None, underline, bold}
        public enum ImageAlign { None, left, center, right }
        public enum ListStyle {None, bullet, number, alpha }


        protected Tag tag;

        /**
         * @fn  public Tag getTag()
         *
         * @brief   Gets the tag.
         *
         * @return  The tag.
         */

        public Tag getTag()
        {
            return tag;
        }

        /**
         * @fn  public static Tag identifyTag(string tagStr)
         *
         * @brief   Identify tag.
         *
         * @param   tagStr  The tag string.
         *
         * @return  A Tag.
         */

        public static Tag identifyTag(string tagStr)
        {
            tagStr = tagStr.ToLower();
            switch (tagStr)
            {
                case "h1":
                    return Tag.h1;
                case "p":
                    return Tag.p;
                case "img":
                    return Tag.img;
                case "ul":
                    return Tag.ul;
                case "li":
                    return Tag.li;
                default:
                    return Tag.None;
            }
        }

        /**
         * @fn  public static TextStyle identifyTextStyle(string styleStr)
         *
         * @brief   Identify text style.
         *
         * @param   styleStr    The style string.
         *
         * @return  A TextStyle.
         */

        public static TextStyle identifyTextStyle(string styleStr)
        {
            styleStr = styleStr.ToLower();
            switch (styleStr)
            {
                case "underline":
                    return TextStyle.underline;
                case "bold":
                    return TextStyle.bold;
                default:
                    return TextStyle.None;
            }
        }

        /**
         * @fn  public static ImageAlign identifyImageAlign(string alignStr)
         *
         * @brief   Identify image align.
         *
         * @param   alignStr    The align string.
         *
         * @return  An ImageAlign.
         */

        public static ImageAlign identifyImageAlign(string alignStr)
        {
            alignStr = alignStr.ToLower();
            switch (alignStr)
            {
                case "left":
                    return ImageAlign.left;
                case "center":
                    return ImageAlign.center;
                case "right":
                    return ImageAlign.right;
                default:
                    return ImageAlign.None;
            }
        }

        /**
         * @fn  public static ListStyle identifyListStyle(string styleStr)
         *
         * @brief   Identify list style.
         *
         * @param   styleStr    The style string.
         *
         * @return  A ListStyle.
         */

        public static ListStyle identifyListStyle(string styleStr)
        {
            styleStr = styleStr.ToLower();
            switch (styleStr)
            {
                case "bullet":
                    return ListStyle.bullet;
                case "number":
                    return ListStyle.number;
                case "alpha":
                    return ListStyle.alpha;
                default:
                    return ListStyle.None;
            }
        }

    }

}
