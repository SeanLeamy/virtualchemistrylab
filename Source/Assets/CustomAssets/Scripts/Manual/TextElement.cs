﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.CustomAssets.Scripts.Manual
{
    class TextElement : ManualElement
    {
        string text;
        bool underline;
        bool bold;

        /**
         * @fn  public TextElement(ManualElement.Tag tag, string text, string styles)
         *
         * @brief   Constructor.
         *
         * @param   tag     The tag.
         * @param   text    The text.
         * @param   styles  The styles.
         */

        public TextElement(ManualElement.Tag tag, string text, string styles)
        {
            if (tag != ManualElement.Tag.h1 && tag != ManualElement.Tag.p)
                return;
            this.tag = tag;
            this.text = text;
            string[] stylesArray = styles.Split(';');
            foreach(string style in stylesArray)
            {
                ManualElement.TextStyle textStyle = ManualElement.identifyTextStyle(style);
                switch (textStyle)
                {
                    case ManualElement.TextStyle.underline:
                        underline = true;
                        break;
                    case ManualElement.TextStyle.bold:
                        bold = true;
                        break;
                    default:
                        break;
                }
            }
        }

        /**
         * @fn  public bool isH1()
         *
         * @brief   Query if this object is h 1.
         *
         * @return  True if h 1, false if not.
         */

        public bool isH1()
        {
            if (tag == ManualElement.Tag.h1)
                return true;
            return false;
        }

        /**
         * @fn  public bool isP()
         *
         * @brief   Query if this object is p.
         *
         * @return  True if p, false if not.
         */

        public bool isP()
        {
            if (tag == ManualElement.Tag.p)
                return true;
            return false;
        }

        /**
         * @fn  public string getText()
         *
         * @brief   Gets the text.
         *
         * @return  The text.
         */

        public string getText()
        {
            return text;
        }

        /**
         * @fn  public bool isUnderline()
         *
         * @brief   Query if this object is underline.
         *
         * @return  True if underline, false if not.
         */

        public bool isUnderline()
        {
            return underline;
        }

        /**
         * @fn  public bool isBold()
         *
         * @brief   Query if this object is bold.
         *
         * @return  True if bold, false if not.
         */

        public bool isBold()
        {
            return bold;
        }

    }

}
