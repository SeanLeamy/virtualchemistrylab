﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.CustomAssets.Scripts.Manual
{
    class ImageElement : ManualElement
    {
        ManualElement.ImageAlign alignment;
        string imagePath;
        public float height { get; set; }

        /**
         * @fn  public ImageElement(ManualElement.Tag tag, string imagePath, string imageAlignment)
         *
         * @brief   Constructor.
         *
         * @param   tag             The tag.
         * @param   imagePath       Full pathname of the image file.
         * @param   imageAlignment  The image alignment.
         */

        public ImageElement(ManualElement.Tag tag, string imagePath, string imageAlignment, string height)
        {
            this.height = 0f;
            if (tag != ManualElement.Tag.img)
                return;
            this.tag = tag;
            this.imagePath = imagePath;
            alignment = ManualElement.identifyImageAlign(imageAlignment);
            if(!string.IsNullOrEmpty(height))
                this.height = float.Parse(height);
        }

        /**
         * @fn  public string getImagePath()
         *
         * @brief   Gets image path.
         *
         * @return  The image path.
         */

        public string getImagePath()
        {
            return imagePath;
        }

        /**
         * @fn  public bool isCenter()
         *
         * @brief   Query if this object is center.
         *
         * @return  True if center, false if not.
         */

        public bool isCenter()
        {
            if (alignment == ManualElement.ImageAlign.center)
                return true;
            return false;
        }

        /**
         * @fn  public bool isLeft()
         *
         * @brief   Query if this object is left.
         *
         * @return  True if left, false if not.
         */

        public bool isLeft()
        {
            if (alignment == ManualElement.ImageAlign.left)
                return true;
            return false;
        }

        /**
         * @fn  public bool isRight()
         *
         * @brief   Query if this object is right.
         *
         * @return  True if right, false if not.
         */

        public bool isRight()
        {
            if (alignment == ManualElement.ImageAlign.right)
                return true;
            return false;
        }

    }

}
