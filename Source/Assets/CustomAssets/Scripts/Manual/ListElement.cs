﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.CustomAssets.Scripts.Manual
{
    class ListElement : ManualElement
    {
        ManualElement.ListStyle listStyle;
        List<TextElement> children;

        /**
         * @fn  public ListElement(ManualElement.Tag tag, List<TextElement> children, string listStyle)
         *
         * @brief   Constructor.
         *
         * @param   tag         The tag.
         * @param   children    The children.
         * @param   listStyle   The list style.
         */

        public ListElement(ManualElement.Tag tag, List<TextElement> children, string listStyle)
        {
            if (tag != ManualElement.Tag.ul)
                return;
            this.tag = tag;
            this.listStyle = ManualElement.identifyListStyle(listStyle);
            this.children = children;
        }

        /**
         * @fn  public List<TextElement> getChildren()
         *
         * @brief   Gets the children of this item.
         *
         * @return  The children.
         */

        public List<TextElement> getChildren()
        {
            return children;
        }

        /**
         * @fn  public int getListSize()
         *
         * @brief   Gets list size.
         *
         * @return  The list size.
         */

        public int getListSize()
        {
            return children.Count;
        }

        /**
         * @fn  public bool isNumber()
         *
         * @brief   Query if this object is number.
         *
         * @return  True if number, false if not.
         */

        public bool isNumber()
        {
            if (listStyle == ManualElement.ListStyle.number)
                return true;
            return false;
        }

        /**
         * @fn  public bool isBullet()
         *
         * @brief   Query if this object is bullet.
         *
         * @return  True if bullet, false if not.
         */

        public bool isBullet()
        {
            if (listStyle == ManualElement.ListStyle.bullet)
                return true;
            return false;
        }

        /**
         * @fn  public bool isAlpha()
         *
         * @brief   Query if this object is alpha.
         *
         * @return  True if alpha, false if not.
         */

        public bool isAlpha()
        {
            if (listStyle == ManualElement.ListStyle.alpha)
                return true;
            return false;
        }



    }

}
