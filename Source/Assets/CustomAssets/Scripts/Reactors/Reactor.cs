﻿using UnityEngine;
using System.Collections.Generic;

public abstract class Reactor : MonoBehaviour {

    protected Container container;
    private float time;
    private int nextUpdate = 1;
    bool reactionEnabled;

    Dictionary<Material, float> materials;

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

    void Start () {
        container = gameObject.GetComponent<Container>();
        reactionEnabled = true;
	}

    /**
     * @fn  void Update ()
     *
     * @brief   Update is called once per frame.
     */

	void Update () {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            if (reactionEnabled)
            {
                time++;
                PeriodicCalls();
            }
        }
    }

    /**
     * @fn  void PeriodicCalls()
     *
     * @brief   Call dependent on nextUpdate interval (default 1 second)
     */

    void PeriodicCalls()
    {
        UpdateMaterials();
        React();
    }

    /**
     * @fn  void UpdateMaterials()
     *
     * @brief   Updates the materials.
     */

    void UpdateMaterials()
    {
        materials = container.getMaterials();
    }

    /**
     * @fn  protected abstract void React();
     *
     * @brief   Reacts this object.
     */

    protected abstract void React();

    /**
     * @fn  public void StopReaction()
     *
     * @brief   Stops a reaction.
     */

    public void StopReaction()
    {
        reactionEnabled = false;
    }

    /**
     * @fn  public void StartReaction()
     *
     * @brief   Starts a reaction.
     */

    public void StartReaction()
    {
        reactionEnabled = true;
    }

}
