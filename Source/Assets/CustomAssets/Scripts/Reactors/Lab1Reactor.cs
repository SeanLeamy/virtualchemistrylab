﻿using UnityEngine;
using System.Collections;
using System;

public class Lab1Reactor : Reactor {
   
    
    public float substrateConcentration { get; set; }
    public float initialReactionVelocity { get; set; }
    public float reactionVelocity { get; set; }
    public float product { get; set; }

    /**
     * @fn  protected override void React()
     *
     * @brief   Reacts this object.
     */

    protected override void React()
    {
        
        




    }

    /**
     * @fn  void Start()
     *
     * @brief   Use this for initialization.
     */

    void Start()
    {

    }

    /**
     * @fn  void Update()
     *
     * @brief   Update is called once per frame.
     */

    void Update()
    {
            
    }

}
