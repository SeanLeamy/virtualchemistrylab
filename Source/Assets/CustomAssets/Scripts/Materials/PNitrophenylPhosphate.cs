﻿using UnityEngine;
using System.Collections;
using System;

// This materal is a Substrate
public class PNitrophenylPhosphate : Material {


    public float substrateConcentration { get; set; } // milliMeter - Concentration of substrate

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {
        panelTitle = "P-Nitrophenyl Phosphatase";
        panelDesc = "Pour (mL)";
        spritePath = "Sprites/Equipment/bottle";
        materialSprite = Resources.Load<Sprite>(spritePath);
        liquidColor = new Color(116 / 255f, 204 / 255f, 244 / 255f, 0.6f);
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {
        materialName = "P-Nitrophenyl Phosphatase " + substrateConcentration + " μM";
    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<PNitrophenylPhosphate>();
        equipmentGameObject.AddComponent<Draggable>();
    }

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {
        
    }

}
