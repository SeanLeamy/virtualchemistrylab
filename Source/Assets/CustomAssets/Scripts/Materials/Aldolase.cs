﻿using UnityEngine;
using System.Collections;

// This materal is an enzyme
public class Aldolase : Material
{

    public float enzymeConcentration { get; set; }    // milligram / milliLitre - Concentration of enzyme
    public float phosphateConcentration { get; set; } // milliMeter - Concetration of phoshateBuffer
    public float MgCl { get; set; }                   // milliMeter - Concentration of Magnesium chloride cofactor
    public float pH { get; set; }

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {

    }

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {

        panelTitle = "Aldolase";
        panelDesc = "Pour (mL)";
        spritePath = "Sprites/Equipment/bottle";
        materialSprite = Resources.Load<Sprite>(spritePath);
        liquidColor = new Color(116 / 255f, 204 / 255f, 244 / 255f, 0.6f);
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {
        materialName = "Aldolase " + enzymeConcentration + " mg/mL";
    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<Aldolase>();
        equipmentGameObject.AddComponent<Draggable>();
    }
}
