﻿
using UnityEngine;
using System.Collections;

public class Water : Material {

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {
        
    }

    /**
     * @fn  public void pour(float amount)
     *
     * @brief   Pours.
     *
     * @param   amount  The amount.
     */

    public void pour(float amount)
    {

    }

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {
        panelTitle = "Water";
        panelDesc = "Pour (mL)";
        spritePath = "Sprites/Equipment/bottle";
        materialSprite = Resources.Load<Sprite>(spritePath);
        materialName = "Water";
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {

    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<Water>();
        equipmentGameObject.AddComponent<Draggable>();
    }



}
