﻿using UnityEngine;
using System.Collections;

public class PNitrophenol : Material
{


    public float productConcentration { get; set; } // milliMeter - Concentration of Product (micro-mol per milli litre)
    public float absorbance;

    private float absorbanceAtMaxColour = 1.261f; // absorbance value for opaque yellow

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {
        panelTitle = "P-Nitrophenol";
        panelDesc = "Pour (mL)";
        spritePath = "Sprites/Equipment/bottle";
        materialSprite = Resources.Load<Sprite>(spritePath);
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {
        materialName = "p-nitrophenol " + productConcentration + " mM (μmol/mL)";
        absorbance = ((1.262f * productConcentration) - 0.001f);  // y = 1.262x - 0.001
        float transparency = (absorbance / absorbanceAtMaxColour);
        if (transparency > 0.8)
            transparency = 0.8f; // Maxium transparency
        liquidColor = new Color(255 / 255f, 255 / 255f,  0 / 255f, transparency);
    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<PNitrophenylPhosphate>();
        equipmentGameObject.AddComponent<Draggable>();
    }

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {

    }

}
