﻿using UnityEngine;
using System.Collections;
using System;

// This materal is a protein
public class BovineSerumAlbumin : Material
{

    public float mgmL { get; set; }            // milligram / milliLitre
    public float phosphateBuffer { get; set; } // milliMeter
    public float MgCl { get; set; }            // milliMeter
    public float pH { get; set; }

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {

    }

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {
        panelTitle = "Bovine Serum Albumin";
        panelDesc = "Pour (mL)";
        spritePath = "Sprites/Equipment/bottle";
        materialSprite = Resources.Load<Sprite>(spritePath);
        liquidColor = new Color(116 / 255f, 204 / 255f, 244 / 255f, 0.6f);
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {
        materialName = "Bovine Serum Albumin " + mgmL + " mg/mL";
    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<BovineSerumAlbumin>().initDynamicMembers();
        equipmentGameObject.AddComponent<Draggable>();
    }
}

