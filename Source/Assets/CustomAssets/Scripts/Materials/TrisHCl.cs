﻿using UnityEngine;
using System.Collections;

// This materal is a Reaction buffer
public class TrisHCl : Material
{


    public float reactionConcentration { get; set; } // milliMeter - Concentration of reaction buffer
    public float pH { get; set; }
    public float MgCl2Concentration { get; set; } // milliMeter - mangnesium chloride concentration

    /**
     * @fn  public override void initStaticMembers()
     *
     * @brief   Init static members.
     */

    public override void initStaticMembers()
    {
        panelTitle = "Tris-HCl";
        panelDesc = "Pour (mL)";
        spritePath = "Sprites/Equipment/bottle";
        materialSprite = Resources.Load<Sprite>(spritePath);
        liquidColor = new Color(116 / 255f, 204 / 255f, 244 / 255f, 0.6f);
    }

    /**
     * @fn  public override void initDynamicMembers()
     *
     * @brief   Init dynamic members.
     */

    public override void initDynamicMembers()
    {
        materialName = reactionConcentration + " M Tris-HCl " + pH + " pH " + MgCl2Concentration + " mM MgCl2";
    }

    /**
     * @fn  public override void addToGameObject(GameObject equipmentGameObject)
     *
     * @brief   Adds to the game object.
     *
     * @param   equipmentGameObject The equipment game object.
     */

    public override void addToGameObject(GameObject equipmentGameObject)
    {
        equipmentGameObject.AddComponent<TrisHCl>();
        equipmentGameObject.AddComponent<Draggable>();
    }

    /**
     * @fn  public override void showConfigureMenu()
     *
     * @brief   Shows the configure menu.
     */

    public override void showConfigureMenu()
    {

    }

}

