﻿using UnityEngine;
using System.Collections;

public abstract class Material : MonoBehaviour {

    // Material
    public string spritePath;
    public Sprite materialSprite;
    public string materialName;
    public float spriteScale;
    public Color liquidColor { get; set; }
    // Settings UI
    protected UIController uiController;
    protected GameObject uiPanel;
    protected GameObject uiContentArea;
    protected string panelDesc;
    protected string panelTitle;
    protected float panelSizeW;
    protected float panelSizeH;

    /**
     * @fn  void Awake()
     *
     * @brief   Awakes this object.
     */

    void Awake()
    {
        initStaticMembers();
        if (uiController == null)
        {
            uiController = GameObject.Find("Utils").GetComponent<UIController>();
        }
    }

    /**
     * @fn  void Start ()
     *
     * @brief   Use this for initialization.
     */

    void Start () {
        initDynamicMembers();
    }

    /**
     * @fn  abstract public void initStaticMembers();
     *
     * @brief   Init static members.
     */

    abstract public void initStaticMembers();

    /**
     * @fn  abstract public void initDynamicMembers();
     *
     * @brief   Init dynamic members.
     */

    abstract public void initDynamicMembers();

    /**
     * @fn  abstract public void showConfigureMenu();
     *
     * @brief   Shows the configure menu.
     */

    abstract public void showConfigureMenu();

    /**
     * @fn  public void closeConfigureMenu()
     *
     * @brief   Closes configure menu.
     */

    public void closeConfigureMenu()
    {
        if (uiPanel == null)
            return;
        Destroy(uiPanel);
    }

    /**
     * @fn  public GameObject getConfigPanel()
     *
     * @brief   Gets configuration panel.
     *
     * @return  The configuration panel.
     */

    public GameObject getConfigPanel()
    {
        return uiPanel;
    }

    /**
     * @fn  public string getMaterialName()
     *
     * @brief   Gets material name.
     *
     * @return  The material name.
     */

    public string getMaterialName()
    {
        return materialName;
    }

    /**
     * @fn  public Sprite getMaterialSprite()
     *
     * @brief   Gets material sprite.
     *
     * @return  The material sprite.
     */

    public Sprite getMaterialSprite()
    {
        return materialSprite;
    }

    /**
     * @fn  public float getSpriteScale()
     *
     * @brief   Gets sprite scale.
     *
     * @return  The sprite scale.
     */

    public float getSpriteScale()
    {
        return spriteScale;
    }

    /**
     * @fn  abstract public void addToGameObject(GameObject gameObject);
     *
     * @brief   Adds to the game object.
     *
     * @param   gameObject  The game object.
     */

    abstract public void addToGameObject(GameObject gameObject);

}
